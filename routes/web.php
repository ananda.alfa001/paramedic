<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\DiabetesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\GuardController;
use App\Http\Controllers\Multiuser\DataPxMultiController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\DataPxAdminController;
use App\Http\Controllers\Pegawai\PegawaiController;
use App\Http\Controllers\Pegawai\DataPxPegawaiController;
use App\Http\Controllers\Apotek\ApotekController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::fallback(function () {
//     Alert::Error('Link Tidak Tersedia', 'Cek Kembali!');
//     return redirect()->back();
// });

Route::group(['middleware' => 'guest'], function () {
    Route::get('/',[GuestController::class,'login'])->name('login');
    Route::get('/register',[GuestController::class,'register'])->name('register');
    Route::post('/login',[LoginController::class,'authenticate'])->name('authenticate');
    Route::post('/register-guest',[RegisterController::class,'store'])->name('register-guest');
});

Route::middleware(['auth'])->group(function () {
    //Session Guard
    Route::get('/Auth-Roles',[GuardController::class,'AuthRoles'])->name('AuthRoles');
    Route::get('/Guard',[GuardController::class,'AuthGuard'])->name('AuthGuard');
    //logout
    Route::get('/logout',[LoginController::class,'logout'])->name('dashboard-logout');
    //ide useless but realibility dashboard
    Route::get('/dashboard', [DataPxMultiController::class, 'Dashboard_Data'])->name('Dashboard_Data');
    // Ini Untuk Daftar Berobat
    Route::get('/dashboard/daftar/{id}', [DataPxMultiController::class, 'daftar_berobat'])->name('daftar-berobat');
    // Ini Untuk Tambah Pasien baru Aja
    Route::get('/dashboard/patientregist',[DataPxMultiController::class,'tambah'])->name('registrasi-pasien');
    Route::post('/dashboard/patientregist/send',[DataPxMultiController::class,'store'])->name('sent-registrasi-pasien');
    // Ini untuk Tambah pasien baru + Daftar
    Route::get('/dashboard/patientsign',[DataPxMultiController::class,'daftar'])->name('dashboard-daftar');
    // Route::post('/dashboard/patientsign/send',[DataPxMultiController::class,'queue'])->name('dashboard-daftar-sent');
    // Mengubah Status
    Route::get('/dashboard/patientsign/statuswait/{id}', [DataPxMultiController::class, 'upstatuswait'])->name('update-pasien-menunggu');
    Route::get('/dashboard/patientsign/statusprogress/{id}', [DataPxMultiController::class, 'upstatusprogress'])->name('update-pasien-pemeriksaan');
    Route::get('/dashboard/patientsign/statusdone/{id}', [DataPxMultiController::class, 'upstatusdone'])->name('update-pasien-selesai');
    // Ini untuk kebutuhan Rekam Medis
    Route::get('/dashboard/rekammedis',[DataPxMultiController::class,'remed'])->name('rekam-medis');
    Route::get('/dashboard/kohort',[DataPxMultiController::class,'kohort'])->name('kohort');
    Route::get('/dashboard/kohort/validation/{id}',[DataPxMultiController::class,'kohortvalid'])->name('kohort-valid');
    // Ini untuk ibu hamil
    Route::get('/dashboard/ibu_hamil/{id}',[DataPxMultiController::class,'gotoibuhamil'])->name('ibu-hamil');
    Route::post('/dashboard/ibu_hamil/send',[DataPxMultiController::class,'setibuhamil'])->name('set-ibuhamil');
    Route::post('/dashboard/ibu_hamil/validate',[DataPxMultiController::class,'validateibuhamil'])->name('validate-ibuhamil');
    // Export to EXCEL
    Route::get('/export-KohortIbu/{id}', [DataPxMultiController::class,'export_RegisterKohortIbuExport']);
    Route::get('/export-RekamMedis', [DataPxMultiController::class,'export_RekamMedisExport']);
    Route::get('/export-invoicepbm', [DataPxMultiController::class,'export_invoicepbm']);
    Route::get('/export-invoiceapotek', [DataPxMultiController::class,'export_invoiceapotek']);
    Route::get('/export-kartustok', [DataPxMultiController::class,'export_kartustok']);
    Route::get('/export-stokapotek', [DataPxMultiController::class,'export_stokapotek']);
    // Route::get('/dashboard/cashier',[DataPxMultiController::class,'gotokasir'])->name('kasir');
});

Route::group(['middleware' => 'admin'], function () {
    //login redirect
    Route::get('/dashboard-admin',[AdminController::class,'index'])->name('dashboard-admin');
    //cek daftar patien
    Route::get('/dashboard-admin/patienttable',[DataPxAdminController::class,'index'])->name('dashboard-tabel');
    // Ini Untuk Edit Data Pasien
    Route::get('/dashboard-admin/hapus/{id}',[DataPxAdminController::class,'hapus'])->name('dashboard-delete');
    Route::get('/dashboard-admin/patientedit/{id}',[DataPxAdminController::class,'edit'])->name('dashboard-edit');
    Route::post('/dashboard-admin/patientedit/update/{id}',[DataPxAdminController::class,'update_profile'])->name('dashboard-update');
    // Kumpulan Table
    Route::get('/dashboard-admin/invoicepbm',[DataPxPegawaiController::class,'gotoinvoice'])->name('dashboard-invoicepbm');
    Route::get('/dashboard-admin/invoiceapotek',[ApotekController::class,'gotoinvoice'])->name('dashboard-invoiceapotek');
    Route::get('/dashboard-admin/rekammedis',[DataPxMultiController::class,'remed'])->name('dashboard-rekammedis');
    Route::get('/dashboard-admin/kohort',[DataPxMultiController::class,'kohort'])->name('dashboard-kohort');
    Route::get('/dashboard-admin/kartustok',[DataPxAdminController::class,'gotokartustok'])->name('dashboard-kartustok');
    Route::get('/dashboard-admin/stokapotek',[ApotekController::class,'index'])->name('dashboard-stokapotek');

});

Route::group(['middleware' => 'pegawai'], function () {
    //login redirect
    Route::get('/dashboard-pegawai',[PegawaiController::class,'index'])->name('dashboard-pegawai');
    // Ini untuk kebutuhan Kasir
    Route::get('/dashboard-pegawai/cashier',[DataPxPegawaiController::class,'gotokasir'])->name('kasir');
    Route::post('/dashboard-pegawai/cashier/cetakinvoice',[DataPxPegawaiController::class,'kasir'])->name('kasir-hitung');
    Route::get('/dashboard-pegawai/cashier/invoice',[DataPxPegawaiController::class,'gotoinvoice'])->name('invoice');
    Route::post('/dashboard-pegawai/cashier/invoice/bayar/{id}',[DataPxPegawaiController::class,'invoice'])->name('bayar');
    Route::post('/dashboard-pegawai/cashier/invoice/cetak/{id}',[DataPxPegawaiController::class,'print'])->name('cetak');  
    Route::get('/diabetes/front',[DiabetesController::class,'index'])->name('diabetes-front');  
    Route::get('/diabetes/latihtree/create',[DiabetesController::class,'createlatihtree'])->name('createlatih');  
    Route::post('/diabetes/datalatih/import',[DiabetesController::class,'importdl'])->name('upload-datalatih');  
});

Route::group(['middleware' => 'apoteker'], function () {
    // Main Page
    Route::get('/dashboard-apotek',[ApotekController::class,'index'])->name('dashboard-apotek');
    //Stok Page
    Route::get('/dashboard-apotek/tambahstok',[ApotekController::class,'gotoadddrug'])->name('masuk-obat');
    Route::post('/dashboard-apotek/tambahstok/addstock',[ApotekController::class,'adddrug'])->name('tambah-obat');
    Route::get('/dashboard-apotek/lihatstok',[ApotekController::class,'stock'])->name('lihat-stok');
    Route::get('/dashboard-apotek/lihatstok/updateharga/{id}',[ApotekController::class,'update_harga'])->name('update-harga');
    Route::get('/dashboard-apotek/lihatstok/tambahstok/{id}',[ApotekController::class,'tambahstock'])->name('tambah-stok');
    Route::get('/dashboard-apotek/lihatstok/hapus/{id}',[ApotekController::class,'hapus_obat'])->name('hapus-stok');
    //Cashier Page
    Route::get('/dashboard-apotek/kasir',[ApotekController::class,'gotokasir'])->name('kasir-apotek');
    Route::post('/dashboard-apotek/kasir/add/{id}',[ApotekController::class,'addkasir'])->name('kasir-apotek-tambahtransaksi');
    Route::post('/dashboard-apotek/kasir/delete/{id}',[ApotekController::class,'deletekasir'])->name('kasir-apotek-deletetransaksi');
    Route::post('/dashboard-apotek/kasir/bayar',[ApotekController::class,'sent_invoice'])->name('kasir-apotek-bayar');
    //Invoice Page
    Route::get('/dashboard-apotek/invoice',[ApotekController::class,'gotoinvoice'])->name('invoice-apotek');
    Route::post('/dashboard-apotek/invoice/bayar/{id}',[ApotekController::class,'bayar'])->name('kasir-invoice-bayar');
    Route::post('/dashboard-apotek/invoice/print/{id}',[ApotekController::class,'print'])->name('invoice-obat-print');
});
