<html>

<head>
    <title>Faktur Pembayaran</title>
    <style>
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        #tabel {
            font-size: 15px;
            border-collapse: collapse;
        }

        #tabel td {
            padding-left: 5px;
            border: 1px solid black;
        }

    </style>
</head>

<body style='font-family:tahoma; font-size:8pt;'>
    @foreach ($invoice as $p)

        <center>
            <table style='width:350px; font-size:16pt; font-family:calibri; border-collapse: collapse;' border='0'>
                <td width='70%' align='CENTER' vertical-align:top'><span style='color:black;'>
                        <b>APOTEK</b>
                        </br><b>Mutiara Ar-Rachman</b>
                        </br>Dsn. Pateguhan, RW 1/RT 5, 
                        </br>Tawang Rejo, Pandaan, Pasuruan</span></br>


                    <span style='font-size:12pt'>No. Telp : (0343)634261, {{ $p->created_at }}</span></br>
                </td>
            </table>
            <tr></br>
                <td align='center'>=============================================</br></td>
            </tr>
            <table class="table text-center">
                <tr>
                    <td>Nomor Transaksi</td>
                    <td>: {{$p->Kode_Invoice}}</td>
                </tr>
                <tr>
                    <td>Status Transaksi</td>
                    <td>: {{$p->Status}}</td>
                </tr>
            </table>
            <tr></br>
                <td align='center'>=============================================</br></td>
            </tr>
            <table cellspacing='4' cellpadding='4'
                style='width:350px; font-size:12pt; font-family:calibri;  border-collapse: collapse;' border='0'>
                <thead>
                    <tr>
                        <th scope="col">
                            <font size="4">No.</font>
                        </th>
                        <th scope="col">
                            <font size="4">Nama</font>
                        </th>
                        <th scope="col">
                            <font size="4">Jumlah</font>
                        </th>
                        <th scope="col">
                            <font size="4">Harga</font>
                        </th>
                        <th scope="col">
                            <font size="4">Total Harga</font>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $number = 1;
                        $Total = 0;
                    ?>
                    @foreach ($obat as $q)
                    <tr>
                        <td align=center>
                            <font size="4">{{$number}}</font>
                            <?php 
                                $number++
                            ?>
                        </td>
                        <td align=left>
                            <font size="4">{{$q->Nama_obat}}</font>
                        <td align=left>
                            <font size="4">{{$q->Jumlah}}</font>
                        </td>
                        <td align=center>
                            <font size="4">{{$q->Harga_Jual}}</font>
                        </td>
                        <td align=center>
                            <?php 
                            $harga = $q->Harga_Jual * $q->Jumlah;
                            $Total = $Total+$harga;
                            ?>
                            <font size="4">{{ $harga }}</font>
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
                <tr>
                    <td colspan='4'>
                        <div style='text-align:right; color:black'>Total : </div>
                    </td>
                    <td style='text-align:right; font-size:16pt; color:black'>{{ $Total }}</td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <div style='text-align:right; color:black'>Bayar : </div>
                    </td>
                    <td style='text-align:right; font-size:16pt; color:black'>{{ $p->Bayar }}</td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <div style='text-align:right; color:black'>Kembali : </div>
                    </td>
                    <td style='text-align:right; font-size:16pt; color:black'>{{ $p->Kembalian }}</td>
                </tr>
            </table>
            <table style='width:350; font-size:12pt;' cellspacing='2'>
                <tr></br>
                    <td align='center'>Petugas</br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'>_________________</br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'>****** TERIMAKASIH ******</br></td>
                </tr>
                <tr></br>
                    <td align='center'>****** Semoga Lekas Sembuh ******</br></td>
                </tr>
            </table>
        </center>
    @endforeach
    <script>
        window.print();
    </script>
</body>

</html>
