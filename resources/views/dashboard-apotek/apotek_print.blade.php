<html>

<head>
    <title>Faktur Pembayaran</title>
    <style>
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        #tabel {
            font-size: 15px;
            border-collapse: collapse;
        }

        #tabel td {
            padding-left: 5px;
            border: 1px solid black;
        }

    </style>
</head>

<body style='font-family:tahoma; font-size:8pt;'>
    @foreach ($pasien as $p)

        <center>
            <table style='width:350px; font-size:16pt; font-family:calibri; border-collapse: collapse;' border='0'>
                <td width='70%' align='CENTER' vertical-align:top'><span style='color:black;'>
                        <b>Praktek Mandiri Bidan (PMB) Bidan Amin Hidayati</b>
                        </br>Jl. WR. Supratman, RT.Rw 5/RW.1, Kolawakerejo, Tawang Rejo, Kec. Pandaan, Pasuruan, Jawa
                        Timur 67156</span></br>


                    <span style='font-size:12pt'>No. : (0343)634261, {{ $p->updated_at }}</span></br>
                </td>
            </table>
            <tr></br>
                <td align='center'>=============================================</br></td>
            </tr>
            <table class="table text-center">
                <tr>
                    <td>Nama</td>
                    <td>: {{$p->Nama_Px}}</td>
                </tr>
                <tr>
                    <td>Umur</td>
                    <td>: {{ $age }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>: {{$p->Alamat_Px}}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>: Pria</td>
                </tr>
                <tr>
                    <td>Diagnosa</td>
                    <td>: {{$p->ops_invoice}}</td>
                </tr>
            </table>
            <tr></br>
                <td align='center'>=============================================</br></td>
            </tr>
            <table cellspacing='4' cellpadding='4'
                style='width:350px; font-size:12pt; font-family:calibri;  border-collapse: collapse;' border='0'>
                <thead>
                    <tr>
                        <th scope="col">
                            <font size="4">No.</font>
                        </th>
                        <th scope="col">
                            <font size="4">Nama</font>
                        </th>
                        <th scope="col">
                            <font size="4">Keterangan</font>
                        </th>
                        <th scope="col">
                            <font size="4">Discount%</font>
                        </th>
                        <th scope="col">
                            <font size="4">Total Harga</font>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align=center>
                            <font size="4">1</font>
                        </td>
                        <td align=left>
                            <font size="4">Pemeriksaan</font></td>
                        <td align=left>
                            <font size="4">{{ $p->pengobatan }}</font>
                        </td>
                        <?php
                        $harga_ext = 50000;
                        $harga = 0;
                        if ($p->pengobatan == 'Pemeriksaan Dokter') {
                            $harga = 50000;
                        } elseif ($p->pengobatan == 'Pemeriksaan Bidan') {
                            $harga = 20000;
                        }
                        $harga_total = $harga_ext + $harga;
                        ?>
                        <td align=center>
                            <font size="4">0.0</font>
                        </td>
                        <td align=center>
                            <font size="4">{{ $harga }}</font>
                        </td>
                    </tr>
                    <tr>
                        <td align=center>
                            <font size="4">2</font>
                        </td>
                        <td align=left>
                            <font size="4">Perawatan</font>
                        </td>
                        <td align=Left>
                            <font size="4">Tidak ada</font>
                        </td>
                        <td align=center>
                            <font size="4">0.0</font>
                        </td>
                        <td align=center>
                            <font size="4">0</font>
                        </td>
                    </tr>
                    <?php
                        $number = 3;
                    ?>
                    @foreach ($tindakan as $q)
                    <tr>
                        <td align=center>
                            <font size="4">{{$number}}</font>
                            <?php 
                                $number++
                            ?>
                        </td>
                        <td align=left>
                            <font size="4">Tindakan</font>
                        <td align=left>
                            <font size="4">{{$q->tindakan}}</font>
                        </td>
                        <td align=center>
                            <font size="4">0.0</font>
                        </td>
                        <td align=center>
                            <font size="4">{{ $q->harga }}</font>
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
                <tr>
                    <td colspan='4'>
                        <div style='text-align:right; color:black'>Total : </div>
                    </td>

                    <?php
                    $temp = 50000;
                    $total_harga = $temp + $harga;
                    ?>
                    <td style='text-align:right; font-size:16pt; color:black'>{{ $total_harga }}</td>
                </tr>
            </table>
            <table style='width:350; font-size:12pt;' cellspacing='2'>
                <tr></br>
                    <td align='center'>Petugas</br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'>_________________</br></td>
                </tr>
                <tr></br>
                    <td align='center'></br></td>
                </tr>
                <tr></br>
                    <td align='center'>****** TERIMAKASIH ******</br></td>
                </tr>
                <tr></br>
                    <td align='center'>****** Semoga Lekas Sembuh ******</br></td>
                </tr>
            </table>
        </center>
    @endforeach
    <script>
        window.print();
    </script>
</body>

</html>
