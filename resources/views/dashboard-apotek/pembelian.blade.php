@extends('main.main')
@section('content')
<div class="pagetitle">
        <h1>APOTEK</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Pasien</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Tabel Barang</h5>
                    <table class="table table-borderless datatable">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Golongan Obat</th>
                                <th scope="col">Nama Obat</th>
                                <th scope="col">Satuan</th>
                                <th scope="col">Stock(biji)</th>
                                <th scope="col">Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                            @foreach ($obat as $p)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $p->Golongan }}</td>
                                        <td>{{ $p->Nama_obat }}</td>
                                        <td><select class="custom-select mr-sm-2 mb-4" id="inlineFormCustomSelect" name="sat_obat" required>
                                            <option selected>Pilih...</option>
                                            <option value="Box" >Box</option>
                                            <option value="Botol">Botol</option>
                                            <option value="Strip">Strip</option>
                                            <option value="Tablet">Tablet</option>
                                        </select></td>
                                        <td>{{ $p->Stok }}</td>
                                        <td>
                                            <form action="{{ route('kasir-apotek-tambahtransaksi', $p->id) }}" method="post">
                                                @csrf
                                                <input type="number" class="form-control" name="jumlah" placeholder="Jumlah">
                                                <button type="submit" class="btn btn-success text-center"><i class="bx bx-plus-circle"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Right side columns -->
        <div class="col-lg-5">
            <!-- Website Traffic -->
            <div class="card">
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                        <li class="dropdown-header text-start">
                            <h6>Filter</h6>
                        </li>
                        <li><a class="dropdown-item" href="#">Today</a></li>
                        <li><a class="dropdown-item" href="#">This Month</a></li>
                        <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
                <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Transaksi</h5>
                                <table class="table table-sm">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Obat</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $Total = 0;
                                        ?>
                                        @foreach ($list_obat as $p)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $p->Nama_obat }}</td>
                                                    <td>{{ $p->Jumlah }}
                                                        <form action="{{ route('kasir-apotek-deletetransaksi', $p->id) }}" method="post">
                                                            @csrf
                                                        <button type="submit" class="btn btn-danger text-center"><i class="bx bx-minus-circle"></i></button>
                                                        </form>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $harga = $p->Harga_Jual * $p->Jumlah;
                                                            $Total = $Total+$harga;
                                                            ?>
                                                        {{ $harga }}</td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <form action="{{ route('kasir-apotek-bayar') }}" method="post">
                                    @csrf
                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-2 col-form-label">Total</label>
                                        <div class="col-sm-10">
                                          <input type="number" class="form-control" name="total" value="{{$Total}}" readonly>
                                        </div>
                                      </div>
                                    <div class="d-grid gap-2 mt-3">
                                        <button type="submit" class="btn btn-info text-center">
                                            Bayar
                                        </button>   
                                      </div>
                                </form>
                                
                                
                            </div>
                        </div>
                    </div>
            </div><!-- End Website Traffic -->
        </div><!-- End Right side columns -->
    </div>



    </div>
@endsection