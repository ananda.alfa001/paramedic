@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Kasir</h1>
        <nav>iytrwswe[5]
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
                <li class="breadcrumb-item active">Cashier</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-16">
                    <div class="card recent-sales">

                        <div class="card-body">
                            <h5 class="card-title">Proses Invoice Pasien<span> | Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">ID Tranksaksi</th>
                                        <th scope="col">Total Harga</th>
                                        <th scope="col">Status Bayar</th>
                                        <th scope="col">Cetak Invoice</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>

                                    @foreach ($obat as $p)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $p->Kode_Invoice }}</td>
                                            <td>{{ $p->Total }}</td>
                                            <td>
                                                @if ($p->Status == 'Belum Lunas')
                                                    <button type="button" class="btn btn-warning btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#bayar{{ $p->id }}">Menunggu</button>
                                                @elseif ($p->Status == "Lunas")
                                                    <button type="button" class="btn btn-success btn-sm">Lunas</button>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($p->Status == 'Belum Lunas')
                                                <button type="button" class="btn btn-info btn-sm" disabled>Cetak Invoice</button>
                                                @elseif ($p->Status == "Lunas")
                                                <form action="{{ route('invoice-obat-print', $p->id) }}" target="_blank" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-info btn-sm">Cetak Invoice</button>
                                                </form>
                                                @endif
                                                
                                                
                                            </td>
                                            <div class="modal fade" id="bayar{{ $p->id }}" tabindex="-1">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Invoice
                                                            </h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form action="{{ route('kasir-invoice-bayar', $p->id) }}" method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="row mb-3">
                                                                    <label for="inputnama"
                                                                        class="col-sm-2 col-form-label">Kode Invoice</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control" id="kode_invoice"
                                                                            value="{{ $p->Kode_Invoice }}" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputEmail3"
                                                                        class="col-sm-2 col-form-label">Total Harga</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="number" class="form-control"
                                                                            name="Total" id="a" value="{{ $p->Total }}"
                                                                            readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputEmail3"
                                                                        class="col-sm-2 col-form-label">Bayar</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="number" class="form-control"
                                                                            name="bayar">
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputEmail3"
                                                                        class="col-sm-2 col-form-label">Kembalian</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control"
                                                                            readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Bayar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if (Auth::user()->roles == 'Admin')
                            <a href="/export-invoiceapotek">
                                <div class="d-grid gap-2 mt-3">
                                    <button class="btn btn-success btn-md" type="button"><i
                                            class="bi bi-file-spreadsheet-fill"></i> Export Excel</button>
                                </div>
                            </a>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue py-4">
        <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Alfa Production House - Copyright &copy;
                2021. All rights
                reserved.</small>
        </div>
    </div>
@endsection
