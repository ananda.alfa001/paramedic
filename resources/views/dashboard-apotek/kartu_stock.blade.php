@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>APOTEK</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Pasien</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->

        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Daftar Obat <span>| Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Golongan Obat</th>
                                        <th scope="col">Nama Obat</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Perubahan</th>
                                        <th scope="col">Jumlah Stock</th>
                                        <th scope="col">Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach ($obat as $p)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $p->Golongan }}</td>
                                            <td>{{ $p->Nama_obat }}</td>
                                            <td>{{ $p->Status }}</td>
                                            <td>{{ $p->Perubahan }}</td>
                                            @foreach ($stok as $z)
                                            @if ($p->Nama_obat == $z->Nama_obat)
                                                <td>{{ $z->Stok }}</td>
                                            @endif
                                            @endforeach
                                            <td>{{ $p->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>


                        </div>



                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div><!-- End Left side columns -->
        @if (Auth::user()->roles == 'Admin')
                            <a href="/export-kartustok">
                                <div class="d-grid gap-2 mt-3">
                                    <button class="btn btn-success btn-md" type="button"><i
                                            class="bi bi-file-spreadsheet-fill"></i> Export Excel</button>
                                </div>
                            </a>
                        @endif
        <!-- Right side columns -->
        <div class="col-lg-4">
            <!-- Website Traffic -->
            <div class="card">
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                        <li class="dropdown-header text-start">
                            <h6>Filter</h6>
                        </li>
                        <li><a class="dropdown-item" href="#">Today</a></li>
                        <li><a class="dropdown-item" href="#">This Month</a></li>
                        <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>

            </div><!-- End Website Traffic -->
        </div><!-- End Right side columns -->
    </div>
@endsection
