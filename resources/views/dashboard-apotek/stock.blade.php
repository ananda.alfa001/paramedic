@extends('main.main')
@section('content')
<div class="pagetitle">
        <h1>APOTEK</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Pasien</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->

        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Daftar Obat <span>| Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Golongan Obat</th>
                                        <th scope="col">Nama Obat</th>
                                        <th scope="col">Harga Beli</th>
                                        <th scope="col">Harga Jual</th>
                                        <th scope="col">Stok</th>
                                        <th scope="col">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach ($obat as $p)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $p->Golongan }}</td>
                                                <td>{{ $p->Nama_obat }}</td>
                                                <td>{{ $p->Harga_beli }}</td>
                                                <td>{{ $p->Harga_Jual }}</td>
                                                <td>{{ $p->Stok }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal"
                                                    data-bs-target="#updateharga{{ $p->id }}"><i class="bx bx-chevrons-up"></i> Update Harga</button>
                                                    <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal"
                                                    data-bs-target="#updatestock{{ $p->id }}"><i class="bx bx-plus-circle"></i> Tambah Stock</button>
                                                    <button type="button" class="btn btn-danger btn-sm"data-bs-toggle="modal"
                                                    data-bs-target="#delete{{ $p->id }}"><i class="bx bx-minus-circle"></i> Hapus Obat</button>
                                                </td>
                                                <div class="modal fade" id="updateharga{{ $p->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Update harga Obat</h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal"
                                                                    aria-label="Close"></button>
                                                            </div>
                                                            <form action="lihatstok/updateharga/{{$p->id}}">
                                                              @csrf
                                                            <div class="modal-body">
                                                                    <div class="row px-3">
                                                                        <label class="mr-sm-2">
                                                                            <h6 class="mb-0 text-sm">{{ $p->Nama_obat }}</h6>
                                                                        </label>
                                                                        <textarea class="mb-4" type="text"
                                                                            name="input"
                                                                            placeholder="Masukkan Harga Baru"
                                                                            required></textarea>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Update</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal fade" id="updatestock{{ $p->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Menambahkan Stock Obat
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal"
                                                                    aria-label="Close"></button>
                                                            </div>
                                                            <form action="lihatstok/tambahstok/{{$p->id}}">
                                                              @csrf
                                                            <div class="modal-body">
                                                                    <div class="row px-3">
                                                                        <label class="mr-sm-2">
                                                                            <h6 class="mb-0 text-sm">Stock Saat ini : {{ $p->Stok }}</h6>
                                                                        </label>
                                                                        <textarea class="mb-4" type="text"
                                                                            name="input"
                                                                            placeholder="Masukkan Stock Yang Ditambahkan"
                                                                            required></textarea>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Daftar</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="delete{{ $p->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Menghapus Stock Obat
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal"
                                                                    aria-label="Close"></button>
                                                            </div>
                                                            <form action="lihatstok/hapus/{{$p->id}}">
                                                              @csrf
                                                            <div class="modal-body">
                                                                    <div class="row px-3">
                                                                        <label class="mr-sm-2">
                                                                            <h6 class="mb-0 text-sm">Apakah Ingin Menghapus obat : {{ $p->Nama_obat }}</h6>
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-danger">HAPUS</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            

                        </div>

                        

                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->
        <div class="col-lg-4">
            <!-- Website Traffic -->
            <div class="card">
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                        <li class="dropdown-header text-start">
                            <h6>Filter</h6>
                        </li>
                        <li><a class="dropdown-item" href="#">Today</a></li>
                        <li><a class="dropdown-item" href="#">This Month</a></li>
                        <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
                
            </div><!-- End Website Traffic -->
        </div><!-- End Right side columns -->
    </div>
@endsection