@extends('main.main')
@section('content')
<div class="pagetitle">
    <h1>Tambah Obat baru</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Form tambah obat</li>
      </ol>
    </nav>
  </div><!-- End Page Title -->
        <form action="tambahstok/addstock" method="post">
            @csrf
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Golongan Obat</h6>
                </label>
                <select class="custom-select mr-sm-2 mb-4" id="inlineFormCustomSelect" name="gol_obat" required>
                    <option value="Keras" selected>Keras</option>
                    <option value="Ringan">Ringan</option>
                    <option value="Generic">Generic</option>
                </select>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Nama Obat</h6>
                </label>
                <input class="mb-4" type="text" name="nama_obat" placeholder="Masukkan Nama Obat" required>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Harga Beli</h6>
                </label>
                <input class="mb-4" type="number" name="harga_beli" placeholder="Harga Beli Obat" required>
            </div>

            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Satuan Obat</h6>
                </label>
                <select class="custom-select mr-sm-2 mb-4" id="inlineFormCustomSelect" name="sat_obat" required>
                    <option value="Box" selected>Box</option>
                    <option value="Botol">Botol</option>
                    <option value="Strip">Strip</option>
                    <option value="Tablet">Tablet</option>
                </select>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Jumlah Box</h6>
                </label>
                <input class="mb-4" type="number" name="box" placeholder="Jumlah Box yang Dimasukkan">
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Jumlah Strip/Botol PerBox</h6>
                </label>
                <input class="mb-4" type="number" name="strip" placeholder="Jumlah Strip/Botol perBox yang Dimasukkan">
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Jumlah Tablet/Biji Perstrip</h6>
                </label>
                <input class="mb-4" type="number" name="tablet" placeholder="Jumlah Tablet/Biji perStrip yang Dimasukkan">
            </div>

            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Harga Jual Box</h6>
                </label>
                <input class="mb-4" type="number" name="harga_jual_box" placeholder="Harga Obat Ketika Dijual dalam Box" required>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Harga Jual Strip</h6>
                </label>
                <input class="mb-4" type="number" name="harga_jual_strip" placeholder="Harga Obat Ketika Dijual dalam Strip" required>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Harga Jual Biji</h6>
                </label>
                <input class="mb-4" type="number" name="harga_jual_biji" placeholder="Harga Obat Ketika Dijual dalam Tablet" required>
            </div>

            <div class="row mb-3 px-3">
                <button type="submit" class="btn btn-success text-center" >
                    Tambahkan Obat
                </button>
            </div>
        </form>
@endsection