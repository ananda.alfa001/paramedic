<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>Ar-Rahman Clinic - Berobat Kini Lebih Mudah</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
	@include('main.components.style')
</head>
<body>
	<!-- ======= Header ======= -->
	@include('main.components.navbar')
	<!-- ======= Sidebar ======= -->
	@include('main.components.sidebar')
	<!-- ======= Alert ======= -->
	@include('sweetalert::alert')
	<!-- ============== -->
	 <main id="main" class="main">
		<section class="section dashboard">
			@yield('content')
		</section>
	 </main>
	<!-- ======= Footer ======= -->
	<footer id="footer" class="footer">
		@include('main.components.footer')
	</footer><!-- End Footer -->
	<!-- /page container -->
	@include('main.components.script')
</body>
</html>
