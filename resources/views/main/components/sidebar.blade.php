 <!-- ======= Sidebar ======= -->
 <aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
      <li class="nav-item">
        <a class="nav-link collapsed" href="/dashboard">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->
      
      @if(Auth::user()->roles == "Admin")
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Registrasi Pasien Baru</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/dashboard-admin/patientregist">
              <i class="bi bi-circle"></i><span>Form Registrasi Pasien</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/patientsign">
              <i class="bi bi-circle"></i><span>Form Edit Pasien</span>
            </a>
          </li>
        </ul>
      </li><!-- End Forms Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-layout-text-window-reverse"></i><span>Data Pasien</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/dashboard-admin/patientsign">
              <i class="bi bi-circle"></i><span>Daftar Antrian</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/patienttable">
              <i class="bi bi-circle"></i><span>Data Pasien</span>
            </a>
          </li>
          <li>
            <a href={{route('rekam-medis')}}>
              <i class="bi bi-circle"></i><span>Rekam Medis</span>
            </a>
          </li>
        </ul>
      </li><!-- End Tables Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#table-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Pembukuan</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="table-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/dashboard-admin/invoicepbm">
              <i class="bi bi-circle"></i><span>Invoice PBM</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/invoiceapotek">
              <i class="bi bi-circle"></i><span>Invoice Apotek</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/rekammedis">
              <i class="bi bi-circle"></i><span>Rekam Medis</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/kohort">
              <i class="bi bi-circle"></i><span>Kohort</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/kartustok">
              <i class="bi bi-circle"></i><span>Kartu Stock</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-admin/stokapotek">
              <i class="bi bi-circle"></i><span>Stock Apotek</span>
            </a>
          </li>
        </ul>
      </li><!-- End Forms Nav -->


      @elseif(Auth::user()->roles == "Pegawai")
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Registrasi Pasien Baru</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/dashboard/patientregist">
              <i class="bi bi-circle"></i><span>Form Registrasi Pasien</span>
            </a>
          </li>
        </ul>
      </li><!-- End Forms Nav -->
      
      <li class="nav-item">
        <a class="nav-link collapsed " href="/dashboard/patientsign">
              <i class="bi bi-card-checklist"></i><span>Daftar Antrian</span>
            </a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-layout-text-window-reverse"></i><span>Data Pasien</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/dashboard/rekammedis">
              <i class="bi bi-circle"></i><span>Rekam Medis</span>
            </a>
          </li>
          <li>
            <a href="/dashboard/kohort">
              <i class="bi bi-circle"></i><span>KOHORT</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed " href="/dashboard-pegawai/cashier">
          <i class="bi bi-cash-coin"></i>
          <span>Cashier</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed " href="/dashboard-pegawai/cashier/invoice">
          <i class="bx bx-building"></i>
          <span>Invoice</span>
        </a>
      </li><!-- End Dashboard Nav -->

      @elseif(Auth::user()->roles == "Apoteker")
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-layout-text-window-reverse"></i><span>Data Obat</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/dashboard-apotek/tambahstok">
              <i class="bi bi-circle"></i><span>Tambah Obat Baru</span>
            </a>
          </li>
          <li>
            <a href="/dashboard-apotek/lihatstok">
              <i class="bi bi-circle"></i><span>Lihat / Update Stok</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed " href="/dashboard-apotek/kasir">
          <i class="bi bi-cash-coin"></i>
          <span>Cashier</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed " href="/dashboard-apotek/invoice">
          <i class="bx bx-building"></i>
          <span>Invoice</span>
        </a>
      </li><!-- End Dashboard Nav -->
      @endif
      <li class="nav-item">
        <a class="nav-link collapsed " href="/diabetes/front">
          <i class="bx bx-building"></i>
          <span>Diabetes</span>
        </a>
      </li>

      <li class="nav-heading">Account Profile</li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="users-profile.html">
          <i class="bi bi-person"></i>
          <span>Profile</span>
        </a>
      </li><!-- End Profile Page Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="pages-contact.html">
          <i class="bi bi-envelope"></i>
          <span>Contact</span>
        </a>
      </li><!-- End Contact Page Nav -->
    </ul>
  </aside><!-- End Sidebar-->