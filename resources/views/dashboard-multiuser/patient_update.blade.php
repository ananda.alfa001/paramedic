@extends('main.main')
@section('content')
<div class="pagetitle">
    <h1>Dashboard</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </nav>
  </div><!-- End Page Title -->
  
        @foreach ($pasien as $p)
            <form action="{{ route('dashboard-edit-update', $p->id) }}" method="post">
                @csrf
                <div class="row px-3">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Nama</h6>
                    </label>
                    <input class="mb-4" type="text" name="nama" placeholder="Masukkan nama pasien" value="{{ $p->Nama_Px }}" required>
                </div>
                <div class="row px-3">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Kepala Keluarga</h6>
                    </label>
                    <input class="mb-4" type="text" name="kk" placeholder="Masukkan nama Kk" value="{{ $p->nama_KK }}" required>
                </div>
                <div class="row px-3">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Jenis Kelamin (Perempuan/Laki-Laki)</h6>
                    </label>
                    <input class="mb-4" type="text" name="jenkel" placeholder="Masukkan Jenkel" value="{{ $p->jenis_kelamin }}" required>
                </div>
                <div class="row px-3">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Alamat</h6>
                    </label>
                    <input class="mb-4" type="text" name="alamat" placeholder="Masukkan alamat pasien" value="{{ $p->Alamat_Px }}" required>
                </div>
                <div class="row px-3">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Nomor Telephone</h6>
                    </label>
                    <input class="mb-4" type="number" name="nomor_telp" placeholder="Nomor Telephone" value="{{ $p->Nomor_Telepon }}">
                </div>
                <div class="row px-3">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Tanggal Lahir</h6>
                    </label>
                    <input class="mb-4" type="date" name="tgl_lhr" placeholder="Masukkan tanggal lahir" value="{{ $p->Tanggal_Lahir }}">
                </div>
                <div class="row mb-3 px-3">
                <button type="submit" class="btn btn-success text-center" >
                    Update Data Pasien
                </button>
            </div>
            </form>
        @endforeach
@endsection