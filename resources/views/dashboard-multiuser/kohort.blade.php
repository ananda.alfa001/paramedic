@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Data Kohort Ibu Hamil</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard-admin">Dashboard</a></li>
                <li class="breadcrumb-item">Data Pasien</li>
                <li class="breadcrumb-item active">KOHORT</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">KOHORT</h5>
                        <p>Tempat data KOHORT dari ibu hamil yang pernah berobat disini</p>
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>NIK</th>
                                    <th>Nama Istri</th>
                                    <th>Nama Suami</th>
                                    <th>Alamat</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                ?>

                                @foreach ($pasien as $p)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $p->NIK }}</td>
                                        <td>{{ $p->Nama_Px }}</td>
                                        <td>{{ $p->nama_KK }}</td>
                                        <td>{{ $p->Alamat_Px }}</td>
                                        <td>
                                            @if ($p->status_kohort == '1')
                                                <a href="/dashboard/kohort/validation/{{ $p->id }}"><button
                                                        type="button" class="btn btn-warning btn-sm">Validate</button></a>
                                            @elseif ($p->status_kohort == "2")
                                                <button type="button" class="btn btn-success rounded-pill">Valid</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if (Auth::user()->roles == 'Admin')
                        <h5 class="card-title">Export Data Excel KOHORT Ibu Hamil</h5>
                                <a href="/export-KohortIbu/{{'Mingguan'}}">
                                    <button type="button" class="btn btn-danger">Export Mingguan</button>
                                </a>
                                <a href="/export-KohortIbu/{{'Bulanan'}}">
                                    <button type="button" class="btn btn-warning">Export Bulanan</button>
                                </a>
                                <a href="/export-KohortIbu/{{'Seluruh'}}">
                                    <button type="button" class="btn btn-success">Export Seluruh data</button>
                                </a>
                                @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
