@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Data Diabetes</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard-admin">Dashboard</a></li>
                <li class="breadcrumb-item">Data Pasien</li>
                <li class="breadcrumb-item active">Diabetes</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Import Data Excel Diabetes</h5>
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                            data-bs-target="#uploadlatih">Upload Data Latih</button>
                            <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                            data-bs-target="#uploaduji">Upload Data Uji</button>
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>ID</th>
                                    <th>Gender</th>
                                    <th>Usia</th>
                                    <th>Klasifikasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                ?>

                                @foreach ($dataset as $p)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $p->id }}</td>
                                        <td>{{ $p->Gender }}</td>
                                        <td>{{ $p->Age }}</td>
                                        <td>{{ $p->Class }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="{{route('createlatih')}}">
                        <button type="button" class="btn btn-danger">Create Tree</button></a>
                        <div class="modal fade" id="uploadlatih"
                            tabindex="-1">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Data Latih
                                        </h5>
                                        <button type="button" class="btn-close"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form action="{{ route('upload-datalatih') }}" method="post" enctype="multipart/form-data">
                                      @csrf
                                    <div class="modal-body">
                                            <div class="row px-3">
                                                <label class="mr-sm-2">
                                                    <h6 class="mb-0 text-sm">Data latih : </h6>
                                                </label>
                                                <input class="mb-4" type="file"
                                                    name="data_latih"
                                                    placeholder="Masukkan data latih yang akan digunakan"
                                                    required>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Upload</button>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="uploaduji"
                            tabindex="-1">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Data Uji
                                        </h5>
                                        <button type="button" class="btn-close"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form action="{{ route('upload-datalatih') }}" method="post" enctype="multipart/form-data">
                                      @csrf
                                    <div class="modal-body">
                                            <div class="row px-3">
                                                <label class="mr-sm-2">
                                                    <h6 class="mb-0 text-sm">Data latih : </h6>
                                                </label>
                                                <input class="mb-4" type="file"
                                                    name="data_latih"
                                                    placeholder="Masukkan data latih yang akan digunakan"
                                                    required>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Upload</button>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
