@extends('main.main')
@section('content')
<div class="pagetitle">
    <h1>Form Registrasi Pasien</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item">Registrasi Pasien Baru</li>
        <li class="breadcrumb-item active">Form Registrasi Pasien</li>
      </ol>
    </nav>
  </div><!-- End Page Title -->
        <form action="patientregist/send" method="post">
            @csrf
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Nama</h6>
                </label>
                <input class="mb-4" type="text" name="nama" placeholder="Masukkan nama pasien" required>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">NIK (Sesuai KTP)</h6>
                </label>
                <input class="mb-4" type="number" name="NIK" placeholder="Masukkan NIK pasien" required>
            </div>
            <div class="form-row align-items-center">
                <div class="col-auto mb-4">
                    <label class="mr-sm-2">
                        <h6 class="mb-0 text-sm">Jenis Kelamin : </h6>
                    </label>
                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="jenis_kelamin" required>
                        <option value="Laki-Laki" selected>Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
              </div> 
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Nama Kepala Keluarga (Sesuai KK)</h6>
                </label>
                <input class="mb-4" type="text" name="nama_kk" placeholder="Masukkan Nama Kepala Keluarga" required>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Alamat</h6>
                </label>
                <input class="mb-4" type="text" name="alamat" placeholder="Masukkan alamat pasien" required>
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Nomor Telephone</h6>
                </label>
                <input class="mb-4" type="number" name="nomor_telp" placeholder="Nomor Telephone">
            </div>
            <div class="row px-3">
                <label class="mr-sm-2">
                    <h6 class="mb-0 text-sm">Tanggal Lahir</h6>
                </label>
                <input class="mb-4" type="date" name="tgl_lhr" placeholder="Masukkan tanggal lahir">
            </div>
            <div class="row mb-3 px-3">
                <button type="submit" class="btn btn-success text-center" >
                    Daftar Pasien Baru
                </button>
            </div>
        </form>
@endsection