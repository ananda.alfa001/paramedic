@extends('main.main')
@section('content')
        <div class="pagetitle">
            <h1>Data Tables</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item">Data Pasien</li>
                    <li class="breadcrumb-item active">Data Rekam Medis</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->
        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Data Pasien</h5>
                            <p>Disini letak data dari pasien yang lengkap</p>

                            <!-- Table with stripped rows -->
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>Nomor Telephone</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Status</th>
                                        <th>Operation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pasien as $p)
                                        <tr>
                                            <td>{{ $p->id }}</td>
                                            <td>{{ $p->Nama_Px }}</td>
                                            <td>{{ $p->Alamat_Px }}</td>
                                            <td>{{ $p->Nomor_Telepon }}</td>
                                            <td>{{ $p->Tanggal_Lahir }}</td>
                                            @if ($p->Status_Px == 1)
                                                <td><a href="#" class="btn btn-secondary">Menunggu</a></td>
                                            @elseif($p->Status_Px == 2)
                                                <td><a href="#" class="btn btn-info">Penanganan</a></td>
                                            @elseif($p->Status_Px == 3)
                                                <td><a href="#" class="btn btn-success">Selesai</a></td>
                                            @endif
                                            <td>
                                                <a class="btn btn-warning btn-sm"
                                                    href="/dashboard-admin/patientedit/{{ $p->id }}">Edit</a>
                                                <a class="btn btn-danger btn-sm"
                                                    href="/dashboard-admin/hapus/{{ $p->id }}">Hapus</a>
                                            </td>
                                        </tr>
                                        
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </section>
@endsection