@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Daftar Antrian Pasien</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Daftar Antrian Pasien</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <table class="table datatable">
        <thead>
            <tr>
                <th>Nomor</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Nomor Telephone</th>
                <th>Tanggal Lahir</th>
                <th>Keluhan</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            ?>

            @foreach ($pasien as $p)
                @if ($p->Status_Px != 3 && $p->status_keluhan == "belum selesai")
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $p->Nama_Px }}</td>
                        <td>{{ $p->Alamat_Px }}</td>
                        <td>{{ $p->Nomor_Telepon }}</td>
                        <td>{{ $p->Tanggal_Lahir }}</td>
                        <td>{{ $p->keluhan }}</td>
                        <td class="btn-group btn-lg " role="group">
                            @if ($p->Status_Px == 1)
                                <button type="button" class="btn btn-warning">
                                    Menunggu
                                </button>
                                <a href="{{ route('update-pasien-pemeriksaan', $p->id) }}">
                                    <button type="button" class="btn btn-outline-danger" style="width: 140px;">
                                        Penanganan
                                    </button>
                                </a>
                                <a href="{{ route('update-pasien-selesai', $p->id) }}">
                                    <button type="button" class="btn btn-outline-success">
                                        Selesai
                                    </button>
                                </a>
                            @elseif ($p->Status_Px == 2)
                                <a href="{{ route('update-pasien-menunggu', $p->id) }}">
                                    <button type="button" class="btn btn-outline-warning">
                                        Menunggu
                                    </button>
                                </a>
                                <button type="button" class="btn btn-danger" style="width: 140px;">
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true">
                                    </span>
                                    Penanganan
                                </button>
                                <a href="{{ route('update-pasien-selesai', $p->id) }}">
                                    <button type="button"
                                        class="btn btn-outline-success">
                                        Selesai
                                    </button>
                                </a>
                            @elseif (Status_Px == 3)
                                <a href="{{ route('update-pasien-menunggu', $p->id) }}">
                                    <button type="button" class="btn btn-outline-warning">
                                        Menunggu
                                    </button>
                                </a>
                                <a href="{{ route('update-pasien-pemeriksaan', $p->id) }}">
                                    <button type="button" class="btn btn-outline-danger" style="width: 140px;">
                                        Penanganan
                                    </button>
                                </a>
                                <button type="button" class="btn btn-success">
                                    Selesai
                                </button>
                            @endif
                        </td>

                        {{-- @if ($p->Status_Px == 1)
                            <td><a href="#" class="btn btn-warning">Menunggu</a></td>
                        @elseif($p->Status_Px == 2)
                            <td><a href="#" class="btn btn-danger">Penanganan</a></td>
                        @elseif($p->Status_Px == 3)
                            <td><a href="#" class="btn btn-success">Selesai</a></td>
                        @endif --}}
                    </tr>
                @endif

            @endforeach
        </tbody>
    </table>
@endsection
