@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Form Isian Ibu Hamil</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <form action="{{route('validate-ibuhamil')}}" method="post">
        @csrf
        @foreach ($pasien as $p)
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Data Pasien</h5>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">ID Kohort</h6>
                            </label>
                            <input class="mb-4" type="text" name="idkohort" value="{{ $p->id }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">ID Pasien</h6>
                            </label>
                            <input class="mb-4" type="text" name="idpx" value="{{ $p->Id_Px }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Nama Pasien</h6>
                            </label>
                            <input class="mb-4" type="text" name="nama_ibu" value="{{ $p->Nama_Px }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">NIK Pasien</h6>
                            </label>
                            <input class="mb-4" type="text" name="NIK" value="{{ $p->NIK }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Nama Suami</h6>
                            </label>
                            <input class="mb-4" type="text" name="nama_suami" value="{{ $p->nama_KK }}"
                                readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Alamat</h6>
                            </label>
                            <input class="mb-4" type="text" name="alamat" value="{{ $p->Alamat_Px }}" readonly>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-auto mb-4">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Sumber Pembiayaan : </h6>
                                </label>
                                <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="sumber_biaya"
                                    required>
                                    @if($p->Sumber_Biaya == "1"){
                                        <option value="1" selected>Mandiri</option>
                                        <option value="2">BPJS</option>
                                        <option value="3">Lainnya</option>
                                    }
                                    @elseif($p->Sumber_Biaya == "2"){
                                        <option value="1" >Mandiri</option>
                                        <option value="2" selected>BPJS</option>
                                        <option value="3">Lainnya</option>
                                    }
                                    @elseif($p->Sumber_Biaya == "3"){
                                        <option value="1">Mandiri</option>
                                        <option value="2">BPJS</option>
                                        <option value="3" selected>Lainnya</option>
                                    }
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Batas Layout --}}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Skrining</h5>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Usia Ibu</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="usia_ibu" value="{{ $age }}"
                                        readonly>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Usia Kehamilan (Minggu)</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="usia_hamil" value="{{ $p->usia_kehamilan }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Hamil Ke</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="hamil_ke" value="{{ $p->hamil_ke }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Jarak Kehamilan</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="jarak_hamil" value="{{ $p->jarak_hamil }}" required>
                                </div>                                
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">BB/TB</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="bb_tb" value="{{ $p->bb_tb }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Tekanan Darah</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="tekanan_darah" value="{{ $p->tekanan }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">LILA/IMT</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="lila_imt" value="{{ $p->lila_imt }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Status Imunisasi TD</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="status_imunisasi" value="{{ $p->status_imunisasi }}" required>
                                </div>
                                <br>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Keterangan Lanjut Imunisasi TD</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="keterangan_imunisasi" value="{{ $p->keterangan_imunisasi }}" id="sts_imun">
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining PE</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_PE" value="{{ $p->Skrining_PE }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining TB</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_TB" value="{{ $p->Skrining_TB }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining Jiwa</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_Jiwa" value="{{ $p->Skrining_Jiwa }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining Gigi</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_Gigi" value="{{ $p->Skrining_Gigi }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Laboratorium</h5>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Hb</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Hb" value="{{ $p->Lab_Hb }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Golongan Darah</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_goldar" value="{{ $p->Lab_Goldar }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Protein</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Protein" value="{{ $p->Lab_Protein }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Glukosa</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Glukosa" value="{{ $p->Lab_Glukosa }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab HIV</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_HIV" value="{{ $p->Lab_HIV }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Sifilis</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Sifilis" value="{{ $p->Lab_Sifilis }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab HBsAg</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_HBsAg" value="{{ $p->Lab_HBsAg }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab TBC Mikroskop</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_TBC" value="{{ $p->Lab_TBC }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Malaria</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_malaria" value="{{ $p->Lab_Malaria }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Lain-lain</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Lain" value="{{ $p->Lab_Lain }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Resiko Nakes</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="resiko_nakes" value="{{ $p->Resiko_Nakes }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Resiko Masyarakat</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="resiko_masyarakat" value="{{ $p->Resiko_Masyarakat }}" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Tata Laksana Kasus</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="tlk" value="{{ $p->tata_laksana_kasus }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}




                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Kunjungan</h5>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Status Buku KIA</h6>
                                </label>
                                <input class="mb-4" type="text" name="buku_kia" value="{{ $p->Buku_KIA }}" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Keterangan Kunjungan Ibu Hamil</h6>
                                </label>
                                <input class="mb-4" type="text" name="kunjungan_ibu" value="{{ $p->kunjungan_ibu }}" required>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Kelahiran</h5>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Penolong Ibu Hamil Jika Nakes</h6>
                                </label>
                                <input class="mb-4" type="text" name="penolong_nakes" value="{{ $p->penolong_nakes }}" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Penolong Ibu Hamil Jika Dukun</h6>
                                </label>
                                <input class="mb-4" type="text" name="penolong_dukun" value="{{ $p->penolong_dukun }}" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Status Bayi Jika Mati</h6>
                                </label>
                                <input class="mb-4" type="text" name="lahir_mati"  value="{{ $p->lahir_mati }}" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Status Bayi Jika Hidup</h6>
                                </label>
                                <input class="mb-4" type="text" name="lahir_hidup" value="{{ $p->lahir_hidup }}" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Periksa Nifas</h6>
                                </label>
                                <input class="mb-4" type="text" name="jadwal_nifas" value="{{ $p->jadwal_nifas }}" required>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}
                </div>


                <div class="row mb-3 px-3">
                    <button type="submit" class="btn btn-success text-center">
                        Validate
                    </button>
                </div>
        @endforeach
    </form>
@endsection
