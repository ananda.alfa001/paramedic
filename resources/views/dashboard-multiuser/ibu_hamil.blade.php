@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Form Isian Ibu Hamil</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <form action="{{route('set-ibuhamil')}}" method="post">
        @csrf
        @foreach ($pasien as $p)
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Data Pasien</h5>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">ID Pasien</h6>
                            </label>
                            <input class="mb-4" type="text" name="idpx" value="{{ $p->id }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Nama Pasien</h6>
                            </label>
                            <input class="mb-4" type="text" name="nama_ibu" value="{{ $p->Nama_Px }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">NIK Pasien</h6>
                            </label>
                            <input class="mb-4" type="text" name="NIK" value="{{ $p->NIK }}" readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Nama Suami</h6>
                            </label>
                            <input class="mb-4" type="text" name="nama_suami" value="{{ $p->nama_KK }}"
                                readonly>
                        </div>
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Alamat</h6>
                            </label>
                            <input class="mb-4" type="text" name="alamat" value="{{ $p->Alamat_Px }}" readonly>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-auto mb-4">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Sumber Pembiayaan : </h6>
                                </label>
                                <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="sumber_biaya"
                                    required>
                                    <option value="1" selected>Mandiri</option>
                                    <option value="2">BPJS</option>
                                    <option value="3">Lainnya</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Batas Layout --}}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Skrining</h5>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Usia Ibu</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="usia_ibu" value="{{ $age }}"
                                        readonly>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Usia Kehamilan (Minggu)</h6>
                                    </label>
                                    <input type="number" class="mb-4" type="text" name="usia_hamil" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Hamil Ke</h6>
                                    </label>
                                    <input type="number" class="mb-4" type="text" name="hamil_ke" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Jarak Kehamilan(Tahun)</h6>
                                    </label>
                                    <input type="number" class="mb-4" type="text" name="jarak_hamil" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Keluhan <h7 style="color:red; font-weight:600;">( Untuk Mendaftar Berobat )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="keluh" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">BB/TB</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="bb_tb" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Tekanan Darah</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="tekanan_darah" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">LILA/IMT</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="lila_imt" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Status Imunisasi TD</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="status_imunisasi" required>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="sts" data-bs-target="#statuschek">
                                        <label class="form-check-label" for="flexSwitchCheckDefault">Status
                                            Imunisasi</label>
                                    </div>
                                    {{-- UnSolved --}}
                                    {{-- <div id="statuschek">
                                        <script>
                                        if (document.getElementById('#sts').is(':checked')) {
                                            document.getElementById("sts_imun").disabled = false;
                                        } else {
                                            document.getElementById("sts_imun").disabled = true;
                                        }    
                                        </script>
                                    </div> --}}
                                </div>
                                <br>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Keterangan Lanjut Imunisasi TD</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="keterangan_imunisasi" id="sts_imun">
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining Anamnesa PE <h7 style="color:red; font-weight:600;">( berisiko atau tidak )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_PE" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining Anamnesa TB <h7 style="color:red; font-weight:600;">( suspek atau non suspek )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_TB" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining Anamnesa Jiwa <h7 style="color:red; font-weight:600;">( kesehatan jiwa )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_Jiwa" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Skrining Gigi <h7 style="color:red; font-weight:600;">( YA (bila melakukan))</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Skrining_Gigi" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Laboratorium</h5>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Hb</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Hb" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Golongan Darah <h7 style="color:red; font-weight:600;">(A/B/O/AB)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_goldar" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Protein <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Protein" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Glukosa <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Glukosa" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab HIV <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_HIV" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Sifilis <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Sifilis" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab HBsAg <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_HBsAg" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab TBC Mikroskop <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_TBC" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Malaria <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_malaria" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Lab Lain-lain <h7 style="color:red; font-weight:600;">(Bila melakukan pemeriksaan lab lain)</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="Lab_Lain" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Resiko Nakes <h7 style="color:red; font-weight:600;">( tanggal dan skor sesuaikan dg KSPR )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="resiko_nakes" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Resiko Masyarakat <h7 style="color:red; font-weight:600;">( tanggal dan skor sesuaikan dg KSPR )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="resiko_masyarakat" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Tata Laksana Kasus <h7 style="color:red; font-weight:600;">( tanggal dan jenis tindakan )</h7></h6>
                                    </label>
                                    <input class="mb-4" type="text" name="tlk" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}




                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Kunjungan</h5>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Status Buku KIA <h7 style="color:red; font-weight:600;">(+/-)</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="buku_kia" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Keterangan Kunjungan Ibu Hamil <h7 style="color:red; font-weight:600;">(tanggal, tempat pelayanan, kode pelayanan, kondisi ibu, tanda pagar setiap trimester)</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="kunjungan_ibu" required>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Kelahiran</h5>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Penolong Ibu Hamil Jika Nakes <h7 style="color:red; font-weight:600;">(tanggal, tempat, jenis persalinan dan kondisi ibu)</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="penolong_nakes" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Penolong Ibu Hamil Jika Dukun <h7 style="color:red; font-weight:600;">(tanggal, tempat, jenis persalinan dan kondisi ibu)</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="penolong_dukun" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Status Bayi Jika Mati <h7 style="color:red; font-weight:600;">(Tanggal)</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="lahir_mati" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Status Bayi Jika Hidup <h7 style="color:red; font-weight:600;">(berat badan dalam gram, panjang badan dalam centimeter dan jenis kelamin L / P )</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="lahir_hidup" required>
                            </div>
                            <div class="row px-3">
                                <label class="mr-sm-2">
                                    <h6 class="mb-0 text-sm">Periksa Nifas <h7 style="color:red; font-weight:600;">( tanggal kunjungan, kode  pelayanan, kondisi ibu )</h7></h6>
                                </label>
                                <input class="mb-4" type="text" name="jadwal_nifas" required>
                            </div>
                        </div>
                    </div>
                    {{-- Batas Layout --}}
                </div>


                <div class="row mb-3 px-3">
                    <button type="submit" class="btn btn-success text-center">
                        Daftar Pasien
                    </button>
                </div>
        @endforeach
    </form>
@endsection
