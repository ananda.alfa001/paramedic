<html>
<table>
    <thead>
        <tr>
            <th style="background-color: #faf87f;">No.</th>
            <th style="background-color: #faf87f;">Golongan</th>
            <th style="background-color: #faf87f;">Nama Obat</th>
            <th style="background-color: #faf87f;">Harga Beli</th>
            <th style="background-color: #faf87f;">Harga Jual</th>
            <th style="background-color: #faf87f;">Jumlah Stok</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        ?>
        @foreach($transactionResult as $p)
        <tr>
            <td>{{$i++}}.</td>
            <td>{{ $p->Golongan }}</td>
            <td>{{ $p->Nama_obat }}</td>
            <td>'{{ $p->Harga_beli }}</td>
            <td>{{ $p->Harga_Jual }}</td>
            <td>{{ $p->Stok }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

</html>
