<html>
<table>
    <thead>
        <tr>
            <th style="background-color: #faf87f;">No.</th>
            <th style="background-color: #faf87f;">Tanggal Berobat</th>
            <th style="background-color: #faf87f;">Nama Pasien</th>
            <th style="background-color: #faf87f;">NIK Pasien</th>
            <th style="background-color: #faf87f;">Alamat</th>
            <th style="background-color: #faf87f;">Keluhan</th>
            <th style="background-color: #faf87f;">Tindakan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        ?>
        @foreach($transactionResult as $p)
        <tr>
            <td>{{$i++}}.</td>
            <td>{{ $p->created_at }}</td>
            <td>{{ $p->Nama_Px }}</td>
            <td>'{{ $p->NIK }}</td>
            <td>{{ $p->Alamat_Px }}</td>
            <td>{{ $p->keluhan }}</td>
            <td>{{ $p->tindakan }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

</html>
