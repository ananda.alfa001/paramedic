<html>
<table>
    <thead>
        <tr>
            <th style="background-color: #faf87f;">No.</th>
            <th style="background-color: #faf87f;">ID Invoice</th>
            <th style="background-color: #faf87f;">Tanggal Berobat</th>
            <th style="background-color: #faf87f;">Nama Pasien</th>
            <th style="background-color: #faf87f;">Tindakan</th>
            <th style="background-color: #faf87f;">Keterangan Tindakan</th>
            <th style="background-color: #faf87f;">Bayar</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        ?>
        @foreach ($Invoice as $p)
            <tr>
                <td>{{ $i++ }}.</td>
                <td>{{ $p->id }}</td>
                <td>{{ $p->created_at }}</td>
                <td>'{{ $p->Nama_Px }}</td>
                <td>{{ $p->pengobatan }}</td>
                <td>{{ $p->inap }}</td>
                <?php 
                $harga = 0;
                $harga_pemeriksaan = 0;
                if ($p->pengobatan == 'Pemeriksaan Dokter') {
                    $harga_pemeriksaan = 50000;
                } elseif ($p->pengobatan == 'Pemeriksaan Bidan') {
                    $harga_pemeriksaan = 25000;
                }
                $harga_perawatan = 0;
                if ($p->inap == 'Rawat Jalan') {
                    $harga_perawatan = 0;
                }elseif ($p->inap == 'Rawat Inap 1') {
                    $harga_perawatan = 150000;
                }elseif ($p->inap == 'Rawat Inap 2') {
                    $harga_perawatan = 200000;
                }
                $harga = $harga_pemeriksaan + $harga_perawatan;
                ?>
                <td>{{ $harga }}</td>
            </tr>
            @foreach ($Invoice_Tindakan as $q)
                    @if ($q->id_invoice == $p->id)
                    <tr>
                        <td>{{ $i++ }}.</td>
                        <td>{{ $p->id }}</td>
                        <td>{{ $p->created_at }}</td>
                        <td>'{{ $p->Nama_Px }}</td>
                        <td>Tindakan</td>
                        <td>{{ $q->tindakan }}</td>
                        <td>{{ $q->harga }}</td>
                    </tr>
                    @endif

                @endforeach
        @endforeach
    </tbody>
</table>

</html>
