<html>
<table>
    <thead>
    <tr>
        <th style="background-color: #faf87f;">No.</th>
        <th style="background-color: #faf87f;">Nama Pasien</th>
        <th style="background-color: #faf87f;" >NIK Pasien</th>
        <th style="background-color: #faf87f;">Nama Suami</th>
        <th style="background-color: #faf87f;">Alamat</th>
        <th style="background-color: #faf87f;">Sumber Pembiayaan</th>
        <th style="background-color: #7ffa7f;">Usia Ibu</th>
        <th style="background-color: #7ffa7f;">Usia Kehamilan (Minggu)</th>
        <th style="background-color: #7ffa7f;">Hamil Ke-</th>
        <th style="background-color: #7ffa7f;">Jarak Kehamilan</th>
        <th style="background-color: #7ffa7f;">BB/TB</th>
        <th style="background-color: #7ffa7f;">Tekanan Darah</th>
        <th style="background-color: #7ffa7f;">LILA/IMT</th>
        <th style="background-color: #7ffa7f;">Status Imunisasi TD</th>
        <th style="background-color: #7ffa7f;">Keterangan Lanjut Imunisasi TD</th>
        <th style="background-color: #7ffa7f;">Skrining PE</th>
        <th style="background-color: #7ffa7f;">Skrining TB</th>
        <th style="background-color: #7ffa7f;">Skrining Jiwa</th>
        <th style="background-color: #7ffa7f;">Skrining Gigi</th>
        <th style="background-color: #7fa0fa;">Lab Hb</th>
        <th style="background-color: #7fa0fa;">Lab Golongan Darah</th>
        <th style="background-color: #7fa0fa;">Lab Protein</th>
        <th style="background-color: #7fa0fa;">Lab Glukosa</th>
        <th style="background-color: #7fa0fa;">Lab HIV</th>
        <th style="background-color: #7fa0fa;">Lab Sifilis</th>
        <th style="background-color: #7fa0fa;">Lab HBsAg</th>
        <th style="background-color: #7fa0fa;">Lab TBC Mikroskop</th>
        <th style="background-color: #7fa0fa;">Lab Malaria</th>
        <th style="background-color: #7fa0fa;">Lab Lain-lain</th>
        <th style="background-color: #7fa0fa;">Resiko Nakes</th>
        <th style="background-color: #7fa0fa;">Resiko Masyarakat</th>
        <th style="background-color: #7fa0fa;">Tata Laksana Kasus</th>
        <th style="background-color: #7ffac1;">Status Buku KIA</th>
        <th style="background-color: #7ffac1;">Keterangan Kunjungan Ibu Hamil</th>
        <th style="background-color: #7ffac1;">Penolong Ibu Hamil Jika Nakes</th>
        <th style="background-color: #7ffac1;">Penolong Ibu Hamil Jika Dukun</th>
        <th style="background-color: #7ffac1;">Status Bayi Jika Mati</th>
        <th style="background-color: #7ffac1;">Status Bayi Jika Hidup</th>
        <th style="background-color: #7ffac1;">Periksa Nifas</th>
    </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
        ?>
        @foreach($pasien as $p)
        <tr>
            <td>{{$i++}}.</td>
            <td>{{ $p->Nama_Px }}</td>
            <td>'{{ $p->NIK }}</td>
            <td>{{ $p->nama_KK }}</td>
            <td>{{ $p->Alamat_Px }}</td>
            <td>
                @if($p->Sumber_Biaya == "1")
                    Mandiri
                @elseif($p->Sumber_Biaya == "2")
                    BPJS
                @elseif($p->Sumber_Biaya == "3")
                    Lainnya
                @endif
            </td>
            {{-- skrining --}}
            <td>{{ $age }}</td>
            <td>{{ $p->usia_kehamilan }}</td>
            <td>{{ $p->hamil_ke }}</td>
            <td>{{ $p->jarak_hamil }}</td>
            <td>{{ $p->bb_tb }}</td>
            <td>{{ $p->tekanan }}</td>
            <td>{{ $p->lila_imt }}</td>
            <td>{{ $p->status_imunisasi }}</td>
            <td>{{ $p->keterangan_imunisasi }}</td>
            <td>{{ $p->Skrining_PE }}</td>
            <td>{{ $p->Skrining_TB }}</td>
            <td>{{ $p->Skrining_Jiwa }}</td>
            <td>{{ $p->Skrining_Gigi }}</td>
            {{-- laboraturium --}}
            <td>{{ $p->Lab_Hb }}"</td>
            <td>{{ $p->Lab_Goldar }}</td>
            <td>{{ $p->Lab_Protein }}</td>
            <td>{{ $p->Lab_Glukosa }}</td>
            <td>{{ $p->Lab_HIV }}</td>
            <td>{{ $p->Lab_Sifilis }}</td>
            <td>{{ $p->Lab_HBsAg }}</td>
            <td>{{ $p->Lab_TBC }}</td>
            <td>{{ $p->Lab_Malaria }}</td>
            <td>{{ $p->Lab_Lain }}</td>
            <td>{{ $p->Resiko_Nakes }}</td>
            <td>{{ $p->Resiko_Masyarakat }}</td>
            <td>{{ $p->tata_laksana_kasus }}</td>
            {{-- kunjungan --}}
            <td>{{ $p->Buku_KIA }}</td>
            <td>{{ $p->kunjungan_ibu }}</td>
            <td>{{ $p->penolong_nakes }}</td>
            <td>{{ $p->penolong_dukun }}</td>
            <td>{{ $p->lahir_mati }}</td>
            <td>{{ $p->lahir_hidup }}</td>
            <td>{{ $p->jadwal_nifas }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</html>