<html>
<table>
    <thead>
        <tr>
            <th style="background-color: #faf87f;">No.</th>
            <th style="background-color: #faf87f;">Golongan</th>
            <th style="background-color: #faf87f;">Nama Obat</th>
            <th style="background-color: #faf87f;">Tanggal Perubahan</th>
            <th style="background-color: #faf87f;">Status</th>
            <th style="background-color: #faf87f;">Perubahan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        ?>
        @foreach($transactionResult as $p)
        <tr>
            <td>{{$i++}}.</td>
            <td>{{ $p->Golongan }}</td>
            <td>{{ $p->Nama_obat }}</td>
            <td>'{{ $p->created_at }}</td>
            <td>{{ $p->Status }}</td>
            <td>{{ $p->Perubahan }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

</html>
