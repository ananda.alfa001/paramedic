<html>
<table>
    <thead>
        <tr>
            <th style="background-color: #faf87f;">No.</th>
            <th style="background-color: #faf87f;">ID Invoice</th>
            <th style="background-color: #faf87f;">Tanggal</th>
            <th style="background-color: #faf87f;">Status</th>
            <th style="background-color: #faf87f;">Total</th>
            <th style="background-color: #faf87f;">Bayar</th>
            <th style="background-color: #faf87f;">Kembalian</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        ?>
        @foreach($transactionResult as $p)
        <tr>
            <td>{{$i++}}.</td>
            <td>{{ $p->Kode_Invoice }}</td>
            <td>{{ $p->created_at }}</td>
            <td>'{{ $p->Status }}</td>
            <td>{{ $p->Total }}</td>
            <td>{{ $p->Bayar }}</td>
            <td>{{ $p->Kembalian }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

</html>
