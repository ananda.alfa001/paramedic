@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Rekam Medis</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item">Data Pasien</li>
                <li class="breadcrumb-item active">Rekam Medis</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Rekam Medis</h5>
                        <p>Data Rekam Medis Pasien yang Terdaftar</p>
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Tanggal Berobat</th>
                                    <th>Keluhan</th>
                                    <th>Terapi</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                ?>
                                @foreach ($pasien as $p)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $p->Nama_Px }}</td>
                                        <td>{{ $p->Alamat_Px }}</td>
                                        <td>{{ $p->created_at }}</td>
                                        <td>{{ $p->keluhan }}</td>
                                        <td>{{ $p->tindakan }}</td>
                                        <td>
                                            @if ($p->status_keluhan == 'belum selesai')
                                                <button type="button" class="btn btn-warning rounded-pill">Menunggu</button>
                                            @elseif ($p->status_keluhan == "selesai")
                                                <button type="button" class="btn btn-success rounded-pill">Selesai</button>
                                            @endif
                                        </td>

                                        {{-- @if ($p->Status_Px == 1)
                                                    <td><a href="#" class="btn btn-warning">Menunggu</a></td>
                                                @elseif($p->Status_Px == 2)
                                                    <td><a href="#" class="btn btn-danger">Penanganan</a></td>
                                                @elseif($p->Status_Px == 3)
                                                    <td><a href="#" class="btn btn-success">Selesai</a></td>
                                                @endif --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if (Auth::user()->roles == 'Admin')
                            <a href="/export-RekamMedis">
                                <div class="d-grid gap-2 mt-3">
                                    <button class="btn btn-success btn-md" type="button"><i
                                            class="bi bi-file-spreadsheet-fill"></i> Export Excel</button>
                                </div>
                            </a>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
