<!doctype html>
<html>

<head>
    <title>Register - Ar-Rachman Clinic</title>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <!-- Favicons -->
    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/login.css') }}" rel="stylesheet" type="text/css">
    <!-- Vendor CSS Files -->
    <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
</head>

<body oncontextmenu='return false' class='snippet-body'>
    <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
        <div class="card card0 border-0">
            <div class="row d-flex">
                <div class="col-lg-6">
                    <div class="card1 pb-5">
                        <div class="row">
                            <img src="assets/img/logo/icon.jpg" class="logo">
                        </div>
                        <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img
                                src="assets/img/logo/kesehatan.png" class="image"> </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card2 card border-0 px-4 py-5">
                        <form method="POST" action="{{ route('register-guest') }}">
                            @csrf
                                <div class="row px-3">
                                    <label class="mr-sm-2">
                                        <h6 class="mb-0 text-sm">Nama</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="name" placeholder="Enter a valid name address" required>
                                </div> 
                                <div class="form-row align-items-center">
                                    <div class="col-auto mb-4">
                                        <label class="mr-sm-2">
                                            <h6 class="mb-0 text-sm">Jabatan</h6>
                                        </label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="roles" required>
                                            <option value="Pegawai" selected>Pegawai</option>
                                            <option value="Apoteker">Apoteker</option>
                                            <option class="text-muted" disabled>Admin</option>
                                        </select>
                                    </div>
                                    <div class="col-auto mb-4">
                                        <label class="mr-sm-2">
                                            <h6 class="mb-0 text-sm">Jenis Kelamin</h6>
                                        </label>
                                        <select class="custom-select mr-sm-4" id="inlineFormCustomSelect" name="gender" required>
                                            <option selected>Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="row px-3">
                                    <label class="mb-1">
                                        <h6 class="mb-0 text-sm">Email</h6>
                                    </label>
                                    <input class="mb-4" type="text" name="email" placeholder="Enter a valid email address" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mb-1">
                                        <h6 class="mb-0 text-sm">Password</h6>
                                    </label>
                                    <input class="mb-4" type="password" name="re-password" placeholder="Enter password" required>
                                </div>
                                <div class="row px-3">
                                    <label class="mb-1">
                                        <h6 class="mb-0 text-sm">Ulangi Password</h6>
                                    </label>
                                    <input class="mb-4" type="password" name="password" placeholder="Enter password Again!" required>
                                </div>
                                @if ($errors->any())
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <strong>Terjadi Kesalahan.</strong><br/>
                                    @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                @endif
                                
                            <div class="row mb-3 px-3">
                                <button type="submit" class="btn btn-info text-center">
                                    Daftar
                                </button>
                            </div>
                        </form>
                        <div class="row mb-4 px-3"> <small class="font-weight-bold">Sudah Punya Akses?
                            <a class="text-danger" href="/">Klik di sini</a></small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-blue py-4">
                <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Alfa Production House - Copyright &copy;
                        2021. All rights
                        reserved.</small>
                    <div class="social-contact ml-4 ml-sm-auto"> <span class="fa fa-facebook mr-4 text-sm"></span> <span
                            class="fa fa-google-plus mr-4 text-sm"></span> <span
                            class="fa fa-linkedin mr-4 text-sm"></span> <span
                            class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span> </div>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'>
    </script>
</body>

</html>
