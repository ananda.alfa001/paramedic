@extends('main.main')
@section('content')
<div class="pagetitle">
        <h1>Dashboard Data Pasien</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Pasien</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Daftar Nama Pasien Laki-laki <span>| Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Nama Kepala Keluarga</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">Nomor Telepon</th>
                                        <th scope="col">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no_L = 1;
                                    $no_P = 1;
                                    ?>
                                    @foreach ($pasien as $p)
                                        @if ($p->jenis_kelamin == "Laki-Laki" && $p->status_byr == 1)
                                            <tr>
                                                <td>{{ $no_L++ }}</td>
                                                <td>{{ $p->Nama_Px }}</td>
                                                <td>{{ $p->nama_KK }}</td>
                                                <td>{{ $p->Alamat_Px }}</td>
                                                <td>{{ $p->Nomor_Telepon }}</td>
                                                <td>
                                                    {{-- Arahkan untuk update status dahulu (bawa id) --}}
                                                    <button type="button" class="btn btn-success btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#daftarkeluh{{ $p->id }}">Daftar Berobat
                                                    </button>
                                                    @if ($p->jenis_kelamin == "Perempuan")                                                        
                                                    <a href="dashboard/ibu_hamil/{{ $p->id }}"><button type="button" class="btn btn-warning btn-sm">Sedang Mengandung</button></a>
                                                    @endif
                                                </td>
                                                
                                                <div class="modal fade" id="daftarkeluh{{ $p->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Daftar Berobat
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal"
                                                                    aria-label="Close"></button>
                                                            </div>
                                                            <form action="dashboard/daftar/{{$p->id}}">
                                                              @csrf
                                                            <div class="modal-body">
                                                                    <div class="row px-3">
                                                                        <label class="mr-sm-2">
                                                                            <h6 class="mb-0 text-sm">Keluhan</h6>
                                                                        </label>
                                                                        <textarea class="mb-4" type="text"
                                                                            name="keluh"
                                                                            placeholder="Masukkan keluhan pasien"
                                                                            required></textarea>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Daftar</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                        

                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div><!-- End Left side columns -->

        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Daftar Nama Pasien Perempuan <span>| Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Nama Kepala Keluarga</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">Nomor Telepon</th>
                                        <th scope="col">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no_L = 1;
                                    $no_P = 1;
                                    ?>
                                    @foreach ($pasien as $p)
                                        @if ($p->Status_Px == 3 && $p->jenis_kelamin == "Perempuan" && $p->status_byr == 1)
                                            <tr>
                                                <td>{{ $no_L++ }}</td>
                                                <td>{{ $p->Nama_Px }}</td>
                                                <td>{{ $p->nama_KK }}</td>
                                                <td>{{ $p->Alamat_Px }}</td>
                                                <td>{{ $p->Nomor_Telepon }}</td>
                                                <td>
                                                    {{-- Arahkan untuk update status dahulu (bawa id) --}}
                                                    <button type="button" class="btn btn-success btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#daftarkeluh{{ $p->id }}">Daftar Berobat
                                                    </button>
                                                    @if ($p->jenis_kelamin == "Perempuan")                                                        
                                                    <a href="dashboard/ibu_hamil/{{ $p->id }}"><button type="button" class="btn btn-warning btn-sm">Sedang Mengandung</button></a>
                                                    @endif
                                                </td>
                                                
                                                <div class="modal fade" id="daftarkeluh{{ $p->id }}"
                                                    tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Daftar Berobat
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal"
                                                                    aria-label="Close"></button>
                                                            </div>
                                                            <form action="dashboard/daftar/{{$p->id}}">
                                                              @csrf
                                                            <div class="modal-body">
                                                                    <div class="row px-3">
                                                                        <label class="mr-sm-2">
                                                                            <h6 class="mb-0 text-sm">Keluhan</h6>
                                                                        </label>
                                                                        <textarea class="mb-4" type="text"
                                                                            name="keluh"
                                                                            placeholder="Masukkan keluhan pasien"
                                                                            required></textarea>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Daftar</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                        

                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->
        <div class="col-lg-4">
            <!-- Website Traffic -->
            <div class="card">
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                        <li class="dropdown-header text-start">
                            <h6>Filter</h6>
                        </li>
                        <li><a class="dropdown-item" href="#">Today</a></li>
                        <li><a class="dropdown-item" href="#">This Month</a></li>
                        <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
                
            </div><!-- End Website Traffic -->
        </div><!-- End Right side columns -->
    </div>
@endsection