@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Kasir</h1>
        <nav>iytrwswe[5]
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
                <li class="breadcrumb-item active">Cashier</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-16">
                    <div class="card recent-sales">

                        <div class="card-body">
                            <h5 class="card-title">Proses Invoice Pasien<span> | Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">Nomor Telepon</th>
                                        <th scope="col">Status Bayar</th>
                                        @if (Auth::user()->roles != 'Admin')
                                            <th scope="col">Cetak Invoice</th>
                                        @endif

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>

                                    @foreach ($pasien as $p)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $p->Nama_Px }}</td>
                                            <td>{{ $p->Alamat_Px }}</td>
                                            <td>{{ $p->Nomor_Telepon }}</td>
                                            <td>
                                                @if ($p->status == 1)
                                                    <button type="button" class="btn btn-warning btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#bayar{{ $p->id }}">Menunggu</button>
                                                @elseif ($p->status == 2)
                                                    <button type="button" class="btn btn-success btn-sm">Lunas</button>
                                                @endif
                                            </td>
                                            @if (Auth::user()->roles != 'Admin')
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm" data-bs-toggle="modal"
                                                        data-bs-target="#cetak{{ $p->id }}">Cetak Invoice</button>
                                                </td>
                                            @endif
                                            {{-- Bayar --}}

                                            <div class="modal fade" id="bayar{{ $p->id }}" tabindex="-1">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Invoice
                                                            </h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form action="{{ route('bayar', $p->id) }}" method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="row mb-3">
                                                                    <label for="inputnama"
                                                                        class="col-sm-2 col-form-label">Nama</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control" id="nama"
                                                                            value="{{ $p->Nama_Px }}" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputEmail3"
                                                                        class="col-sm-2 col-form-label">ID Pasien</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control"
                                                                            name="id_px" value="{{ $p->id_Px }}"
                                                                            readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputalamat"
                                                                        class="col-sm-2 col-form-label">Alamat</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control"
                                                                            id="alamat" value="{{ $p->Alamat_Px }}"
                                                                            disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputttl"
                                                                        class="col-sm-2 col-form-label">Tanggal
                                                                        Lahir</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="date" class="form-control" id="ttl"
                                                                            value="{{ $p->Tanggal_Lahir }}" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputpengobatan"
                                                                        class="col-sm-3 col-form-label">Penanganan</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" class="form-control"
                                                                            id="alamat" value="{{ $p->pengobatan }}"
                                                                            disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputPassword3"
                                                                        class="col-sm-3 col-form-label">Obat dan
                                                                        keterangan</label>
                                                                    <div class="col-sm-9">
                                                                        <textarea class="form-control" type="text"
                                                                            name="ops"
                                                                            disabled>{{ $p->ops_invoice }} </textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Bayar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>


                                            {{-- CETAK INVOICE --}}


                                            <div class="modal fade" id="cetak{{ $p->id }}" tabindex="-1">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Invoice
                                                            </h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form action="{{ route('cetak', $p->id) }}" target="_blank"
                                                            method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="row mb-3">
                                                                    <label for="inputnama"
                                                                        class="col-sm-2 col-form-label">Nama</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control" id="nama"
                                                                            value="{{ $p->Nama_Px }}" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputalamat"
                                                                        class="col-sm-2 col-form-label">Alamat</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control"
                                                                            id="alamat" value="{{ $p->Alamat_Px }}"
                                                                            readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputttl"
                                                                        class="col-sm-2 col-form-label">Tanggal
                                                                        Lahir</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="date" class="form-control" id="ttl"
                                                                            value="{{ $p->Tanggal_Lahir }}" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="inputpengobatan"
                                                                        class="col-sm-3 col-form-label">Penanganan</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" class="form-control"
                                                                            id="alamat" value="{{ $p->pengobatan }}"
                                                                            readonly>
                                                                    </div>
                                                                </div>

                                                                <div class="row mb-3">
                                                                    <label for="inputPassword3"
                                                                        class="col-sm-3 col-form-label">Obat dan
                                                                        keterangan</label>
                                                                    <div class="col-sm-9">
                                                                        <textarea class="form-control" type="text"
                                                                            name="ops"
                                                                            readonly>{{ $p->ops_invoice }} </textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Cetak</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if (Auth::user()->roles == 'Admin')
                                <a href="/export-invoicepbm">
                                    <div class="d-grid gap-2 mt-3">
                                        <button class="btn btn-success btn-md" type="button"><i
                                                class="bi bi-file-spreadsheet-fill"></i> Export Excel</button>
                                    </div>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue py-4">
        <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Alfa Production House - Copyright &copy;
                2021. All rights
                reserved.</small>
        @endsection
