@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Kasir</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
                <li class="breadcrumb-item active">Cashier</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-16">
                    <div class="card recent-sales">

                        <div class="card-body">
                            <h5 class="card-title">Proses Invoice Pasien<span> | Hari ini</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">Nomor Telepon</th>
                                        <th scope="col">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>

                                    @foreach ($pasien as $p)
                                        @if ($p->status_byr == 2 && $p->status_keluhan == 'belum selesai')
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $p->Nama_Px }}</td>
                                                <td>{{ $p->Alamat_Px }}</td>
                                                <td>{{ $p->Nomor_Telepon }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-warning btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#bayar{{ $p->id }}">Bayar</button>
                                                </td>
                                                <div class="modal fade" id="bayar{{ $p->id }}" tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Invoice
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <form action="{{ route('kasir-hitung') }}"
                                                                method="post">
                                                                @csrf
                                                                <div class="modal-body">
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-2 col-form-label">ID Transaksi</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" class="form-control"
                                                                            name="id_transaksi" value="{{ $p->id }}"
                                                                                readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-2 col-form-label">ID Pasien</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" class="form-control"
                                                                            name="id_px" value="{{ $p->id_px }}"
                                                                                readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-2 col-form-label">Nama</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" class="form-control"
                                                                                id="nama" value="{{ $p->Nama_Px }}"
                                                                                disabled>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-2 col-form-label">Alamat</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" class="form-control"
                                                                                id="alamat" value="{{ $p->Alamat_Px }}"
                                                                                disabled>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputPassword3"
                                                                            class="col-sm-2 col-form-label">Tanggal
                                                                            Lahir</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="date" class="form-control"
                                                                                id="ttl" value="{{ $p->Tanggal_Lahir }}"
                                                                                disabled>
                                                                        </div>
                                                                    </div>
                                                                    <fieldset class="row mb-3">
                                                                        <legend class="col-form-label col-sm-2 pt-0">Jenis
                                                                            Pengobatan</legend>
                                                                        <div class="col-sm-10">
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="pemeriksaan" id="gridRadios1"
                                                                                    value="Pemeriksaan Dokter" checked>
                                                                                <label class="form-check-label"
                                                                                    for="gridRadios1">
                                                                                    Pemeriksaan Dokter
                                                                                </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="pemeriksaan" id="gridRadios2"
                                                                                    value="Pemeriksaan Bidan">
                                                                                <label class="form-check-label"
                                                                                    for="gridRadios2">
                                                                                    Pemeriksaan Bidan
                                                                                </label>
                                                                            </div>
                                                                    </fieldset>
                                                                    <fieldset class="row mb-3">
                                                                        <legend class="col-form-label col-sm-2 pt-0">Perawatan</legend>
                                                                        <div class="col-sm-10">
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="perawatan" id="gridRadios1"
                                                                                    value="Rawat Jalan" checked>
                                                                                <label class="form-check-label"
                                                                                    for="gridRadios1">
                                                                                    Rawat Jalan
                                                                                </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="perawatan" id="gridRadios2"
                                                                                    value="Rawat Inap 1">
                                                                                <label class="form-check-label"
                                                                                    for="gridRadios2">
                                                                                    Kamar VK
                                                                                </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="perawatan" id="gridRadios2"
                                                                                    value="Rawat Inap 2">
                                                                                <label class="form-check-label"
                                                                                    for="gridRadios2">
                                                                                    Kamar
                                                                                </label>
                                                                            </div>
                                                                    </fieldset>

                                                                    <div class="row mb-3">
                                                                        <label for="inputPassword3"
                                                                            class="col-sm-3 col-form-label">Obat dan
                                                                            keterangan (Tindakan)</label>
                                                                        <div class="col-sm-9">
                                                                            <textarea class="form-control" type="text"
                                                                                name="ops"
                                                                                placeholder="Masukkan resep obat dan lainnya"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Tindakan :</label>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">USG
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="usg" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Pasang Infus
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="infus" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Spuit
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="spuit" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Incisi
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="incisi" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Rawat Luka
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="rawat_luka" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Jahitan Luka
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="jahit_luka" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Angkat Jahitan
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="angkat_jahit" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Extraksi Kuku
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="kuku" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Pasang /Lepas Cateter
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="cateter" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Khitan
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="khitan" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Persalinan
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="persalinan" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Cairan
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="cairan" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Surat Istirahat
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="surat_istirahat" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Obat Tablet
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="obat_tablet" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Injeksi
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="injeksi" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Suposutoria
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="suposutoria" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Konsultasi
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="number" class="form-control"
                                                                                name="konsultasi" placeholder="Additional Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Laborat
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control"
                                                                                name="infolab" placeholder="Nama Lab">
                                                                            <input type="number" class="form-control"
                                                                                name="lab" placeholder="Lab Price">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                        <label for="inputEmail3"
                                                                            class="col-sm-3 col-form-label">Lain-Lain
                                                                            </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control"
                                                                                name="infolain" placeholder="Lain-lain">
                                                                            <input type="number" class="form-control"
                                                                                name="lain" placeholder="Lain-lain Price">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-bs-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Save
                                                                        changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue py-4">
        <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Alfa Production House - Copyright &copy;
                2021. All rights
                reserved.</small>
        @endsection
