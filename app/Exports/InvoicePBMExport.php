<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;


class InvoicePBMExport implements FromView
{
    public function view(): view
    {
        $Invoice = DB::transaction(function () {
            $pasien = DB::table('invoice as a')
                ->select('a.*', 'b.Nama_Px', 'b.Alamat_Px', 'b.Nomor_Telepon', 'b.Tanggal_Lahir')
                ->leftJoin('pasien as b', 'b.id', 'a.id_px')
                ->get();
            return  $pasien;
        });
        $Invoice_Tindakan = DB::transaction(function () {
            $obat = DB::table('invoice_tindakan')->get();
            return  $obat;
        });
        // DD($transactionResult);
        return view('dashboard-multiuser.export.invoicepbm', compact('Invoice', 'Invoice_Tindakan'));
    }
}
