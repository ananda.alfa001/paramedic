<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;


class KartuStokExport implements FromView
{
    public function view() : view
    {
        $transactionResult = DB::transaction(function(){
            $obat = DB::table('kartu_stok')->get();
            return  $obat;
        });
        // DD($transactionResult);
            return view('dashboard-multiuser.export.kartustok', compact('transactionResult'));  
        }
}
