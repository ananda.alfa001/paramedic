<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;


class StokApotekExport implements FromView
{
    public function view() : view
    {
        $transactionResult = DB::transaction(function(){
            $obat = DB::table('apotek')->get();
            return  $obat;
        });
        // DD($transactionResult);
            return view('dashboard-multiuser.export.stokapotek', compact('transactionResult'));  
        }
}
