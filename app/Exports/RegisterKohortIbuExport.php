<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;

class RegisterKohortIbuExport implements FromView, ShouldAutoSize
{
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view() : view
    {
        $pilihan = $this->id;
        $pasien = DB::transaction(function() use ($pilihan) {
            if($pilihan == "Mingguan"){
                $transactionResult = DB::table('ibu_hamil as a')
                    ->select('a.*', 'b.Nama_Px', 'b.NIK','b.nama_KK','b.Alamat_Px','b.Tanggal_Lahir')
                    ->leftJoin('pasien as b', 'a.Id_Px', 'b.id')
                    ->where('a.updated_at','>=',Carbon::now()->subDays(7))
                    ->get(); 
                    return $transactionResult;
            }elseif($pilihan == "Bulanan"){ 
                $transactionResult = DB::table('ibu_hamil as a')
                    ->select('a.*', 'b.Nama_Px', 'b.NIK','b.nama_KK','b.Alamat_Px','b.Tanggal_Lahir')
                    ->leftJoin('pasien as b', 'a.Id_Px', 'b.id')
                    ->where('a.updated_at','>=',Carbon::now()->subDays(31))
                    ->get(); 
                return $transactionResult;
            }else{
                $transactionResult = DB::table('ibu_hamil as a')
                    ->select('a.*', 'b.Nama_Px', 'b.NIK','b.nama_KK','b.Alamat_Px','b.Tanggal_Lahir')
                    ->leftJoin('pasien as b', 'a.Id_Px', 'b.id')
                    ->get();
                return $transactionResult;
            }
        });
        foreach ($pasien as $p){
            $tgl_lahir = $p -> Tanggal_Lahir;
        }
        $now = Carbon::now(); 
            $b_day = Carbon::parse($tgl_lahir); 
            $age = $b_day->diffInYears($now); 
            return view('dashboard-multiuser.export.RegisterKohortIbuExport', compact('pasien','age')); 
        }
}
