<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;


class RekamMedisExport implements FromView
{
    public function view() : view
    {
        $transactionResult = DB::transaction(function(){
            $pasien = DB::table('keluhan as a')
            ->select('a.*', 'b.Nama_Px', 'b.NIK','b.nama_KK','b.Alamat_Px','b.Tanggal_Lahir')
            ->leftJoin('pasien as b', 'a.Id_Px', 'b.id')
            ->get(); 
            return  $pasien;
        });
        // DD($transactionResult);
            return view('dashboard-multiuser.export.RekamMedisExport', compact('transactionResult'));  
        }
}
