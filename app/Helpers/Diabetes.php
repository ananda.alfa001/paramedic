<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\MitraPeternakanInvited;
use App\Models\HistoryEvent;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;
 
class Diabetes {
    public static function cek($data) {
        return (isNull($data) ? 'Ada isi' : 'Kosong');
    }
    public static function entropygain($fitur,$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total) {
        
        $entropy_total = -1*(($jumlahdatapositif_total/$jumlahdatatotal*(log($jumlahdatapositif_total/$jumlahdatatotal,2)))+($jumlahdatanegatif_total/$jumlahdatatotal*(log($jumlahdatanegatif_total/$jumlahdatatotal,2))));

        $fitur_yesraw = DB::table('diabetes')->where($fitur, '1')->get();
        $fitur_noraw = DB::table('diabetes')->where($fitur, '0')->get();
        
        $fitur_yes_positifraw = DB::table('diabetes')->where($fitur, '1')->where('Class', '1')->get();
        $fitur_yes_negatifraw = DB::table('diabetes')->where($fitur, '1')->where('Class', '0')->get();
        $fitur_no_positifraw = DB::table('diabetes')->where($fitur, '0')->where('Class', '1')->get();
        $fitur_no_negatifraw = DB::table('diabetes')->where($fitur, '0')->where('Class', '0')->get();
        
        $fitur_yes = count($fitur_yesraw);
        $fitur_no = count($fitur_noraw);
        $fitur_yes_positif = count($fitur_yes_positifraw);
        $fitur_yes_negatif = count($fitur_yes_negatifraw);
        $fitur_no_positif = count($fitur_no_positifraw);
        $fitur_no_negatif = count($fitur_no_negatifraw);

        $fitur_entropy_positif = -1*(($fitur_yes_positif/$fitur_yes*(log($fitur_yes_positif/$fitur_yes,2)))+($fitur_yes_negatif/$fitur_yes*(log($fitur_yes_negatif/$fitur_yes,2))));
        
        $fitur_entropy_negatif = -1*(($fitur_no_positif/$fitur_no*(log($fitur_no_positif/$fitur_no,2)))+($fitur_no_negatif/$fitur_no*(log($fitur_no_negatif/$fitur_no,2))));
        
        $fitur_gain = $entropy_total-(($fitur_yes/$jumlahdatatotal*$fitur_entropy_positif)+$fitur_no/$jumlahdatatotal*$fitur_entropy_negatif);

        $entropy = [$fitur_entropy_positif,$fitur_entropy_negatif,$fitur_gain];
        
        return $entropy;
    }
    
}