<?php

namespace App\Imports;

use App\Models\Diabetes;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class DatalatihImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;
    public function model(array $row)
    {
        return new Diabetes([
            'Age' => $row[0],
            'Gender' => $row[1],
            'Polyuria' => $row[2],
            'Polydipsia' => $row[3],
            'S_W_L' => $row[4],
            'Weakness' => $row[5],
            'Polyphagia' => $row[6],
            'Genital_thrus' => $row[7],
            'Visual_blur' => $row[8],
            'Itching' => $row[9],
            'Irritability' => $row[10],
            'Delayed_healing' => $row[11],
            'Partial_paresis' => $row[12],
            'Muscle_stiffness' => $row[13],
            'Alopecia' => $row[14],
            'Obesity' => $row[15],
            'Class' => $row[16],
        ]);
    }
}
