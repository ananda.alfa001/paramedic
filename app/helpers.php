<?php
namespace App;
 
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\MitraPeternakanInvited;
use App\Models\HistoryEvent;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

function cek($data) {
    return (isNull($data) ? 'Ada isi' : 'Kosong');
}
