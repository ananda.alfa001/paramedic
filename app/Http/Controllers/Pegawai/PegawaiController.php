<?php

namespace App\Http\Controllers\Pegawai;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Alert;

class PegawaiController extends Controller
{
    
    public function index()
    {
        if(Auth::user()->roles == "Pegawai"){
            $pasien = DB::table('pasien')->get();
            return view('dashboard-pegawai.pegawai', compact('pasien'));
        }else{
            return redirect('/');
        }
    }
}