<?php

namespace App\Http\Controllers\Pegawai;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DataPxPegawaiController extends Controller
{
    public function gotokasir()
    {
        $pasien = DB::table('keluhan as a')
        ->select('b.*', 'a.keluhan', 'a.status_keluhan', 'a.id', 'a.id_px')
        ->Join('pasien as b', 'b.id', '=' , 'a.id_px')
        ->get();
        // DD($pasien);
        return view('dashboard-pegawai.cashier', compact('pasien'));
    }

    public function gotoinvoice()
    {
        $pasien = DB::table('invoice as a')
        ->select('a.*', 'b.Nama_Px', 'b.Alamat_Px', 'b.Nomor_Telepon', 'b.Tanggal_Lahir')
        ->leftJoin('pasien as b', 'b.id', 'a.id_px')
        ->get();
        // DD($pasien);
        return view('dashboard-pegawai.invoice', compact('pasien'));
    }

    public function kasir(Request $request)
    {
        // $data = $request->all();
        // DD($data);
        // $temp[]=null;
        // $i = 0;
        // foreach ($data as $d){
        //     if($d != null){
        //         $temp[$i] = $d;
        //         $i++;
        //     }
        // }
        // DD($temp);
        $invoice = DB::table('invoice')->insertGetId([
            'id_Px' => $request->id_px,
            'pengobatan' => $request->pemeriksaan,
            'ops_invoice' => $request->ops,
            'inap' => $request->perawatan,
            'created_at' => Carbon::now()
        ]);
        // DD($invoice);

        if($request->infus != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Infus",
                'harga' => $request->infus,
                'created_at' => Carbon::now()
            ]);
        }
        if($request->usg != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Infus",
                'harga' => $request->infus,
                'created_at' => Carbon::now()
            ]);
        }
        if($request->spuit != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Spuit",
                'harga' => $request->spuit,
                'created_at' => Carbon::now()
            ]);
        }if($request->incisi != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Incisi",
                'harga' => $request->incisi,
                'created_at' => Carbon::now()
            ]);
        }if($request->rawat_luka != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Rawat Luka",
                'harga' => $request->rawat_luka,
                'created_at' => Carbon::now()
            ]);
        }if($request->jahit_luka != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Jahit Luka",
                'harga' => $request->jahit_luka,
                'created_at' => Carbon::now()
            ]);
        }if($request->angkat_jahit != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Angkat Jahit",
                'harga' => $request->angkat_jahit,
                'created_at' => Carbon::now()
            ]);
        }if($request->kuku != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Ekstraksi Kuku",
                'harga' => $request->kuku,
                'created_at' => Carbon::now()
            ]);
        }if($request->cateter != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Pasang / Lepas Cateter",
                'harga' => $request->cateter,
                'created_at' => Carbon::now()
            ]);
        }if($request->khitan != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Khirtam / Sunat",
                'harga' => $request->khitan,
                'created_at' => Carbon::now()
            ]);
        }if($request->persalinan != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Persalinan",
                'harga' => $request->persalinan,
                'created_at' => Carbon::now()
            ]);
        }if($request->cairan != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Cairan",
                'harga' => $request->cairan,
                'created_at' => Carbon::now()
            ]);
        }if($request->surat_istirahat != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Surat Istirahat",
                'harga' => $request->surat_istirahat,
                'created_at' => Carbon::now()
            ]);
        }if($request->obat_tablet != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Obat Tablet",
                'harga' => $request->obat_tablet,
                'created_at' => Carbon::now()
            ]);
        }if($request->injeksi != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Injeksi",
                'harga' => $request->injeksi,
                'created_at' => Carbon::now()
            ]);
        }if($request->suposutoria != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Suposutoria",
                'harga' => $request->suposutoria,
                'created_at' => Carbon::now()
            ]);
        }if($request->konsultasi != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => "Konsultasi",
                'harga' => $request->konsultasi,
                'created_at' => Carbon::now()
            ]);
        }if($request->lab != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => $request->infolab,
                'harga' => $request->lab,
                'created_at' => Carbon::now()
            ]);
        }if($request->lain != null){
            DB::table('invoice_tindakan')->insertGetId([
                'id_invoice' => $invoice,
                'tindakan' => $request->infolain,
                'harga' => $request->lain,
                'created_at' => Carbon::now()
            ]);
        }
        
        
        
        
        DB::table('keluhan')->where('id', $request->id_transaksi)->update([
            'status_keluhan' => "selesai",
            'tindakan' => $request->ops,
            'updated_at' => Carbon::now()
        ]);
        return redirect('/dashboard-pegawai/cashier');
    }

    public function invoice(Request $request,$id)
    {
       
        DB::table('invoice')->where('id', $id)->update([
            'status' => 2,
            'updated_at' => Carbon::now()
        ]);
        DB::table('pasien')->where('id', $request->id_px)->update([
            'status_byr' => 1
        ]);
        return redirect('/dashboard-pegawai/cashier/invoice');
    }
    
    public function print($id)
    {
        $pasien = DB::table('invoice as a')->where('a.id', $id)
        ->select('a.*', 'b.Nama_Px', 'b.Alamat_Px', 'b.Nomor_Telepon', 'b.Tanggal_Lahir', 'b.jenis_kelamin', 'c.keluhan')
        ->leftJoin('pasien as b', 'b.id', 'a.id_px')
        ->leftJoin('keluhan as c', 'a.created_at', 'c.updated_at')
        ->get();
        $tindakan = DB::table('invoice_tindakan as b')->where('b.id_invoice', $id)->get();
        foreach ($pasien as $p){
            $tgl_lahir = $p -> Tanggal_Lahir;
        }
        $now = Carbon::now(); 
        $b_day = Carbon::parse($tgl_lahir); 
        $age = $b_day->diffInYears($now); 
        // DD($age);
        return view('dashboard-pegawai.invoice_print', compact('pasien', 'tindakan', 'age'));
    }
}
