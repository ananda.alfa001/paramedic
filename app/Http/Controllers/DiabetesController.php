<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Alert;
use App\Helpers\Diabetes;
use App\Imports\DatalatihImport;

class DiabetesController extends Controller
{
    public function index()
    {
        $dataset = DB::table('diabetes')->get();
        return view('dashboard-multiuser.diabetes_front', compact('dataset'));
    }  
    public function createlatihtree()
    {
        $fitur = ['Age', 'Gender', 'Polyuria', 'Polydipsia', 'S_W_L', 'Weakness', 'Polyphagia', 'Genital_thrus', 'Visual_blur', 'Itching', 'Irritability', 'Delayed_healing', 'Partial_paresis', 'Muscle_Stifness', 'Alopecia', 'Obesity'];
        $fitur_use = [];
        // $fitur1 = mt_rand(0,15);
        // $fitur2 = mt_rand(0,15);
        // $fitur3 = mt_rand(0,15);
        $fitur1 = 4;
        $fitur2 = 3;
        $fitur3 = 14;
        array_push($fitur_use,$fitur1);
        if ($fitur2==$fitur1) {
            $fitur2 = mt_rand(0,16);
        }
        array_push($fitur_use,$fitur2);
        if ($fitur3==$fitur2 || $fitur3==$fitur1) {
            $fitur3 = mt_rand(0,16);
        }
        array_push($fitur_use,$fitur3);

        #Perhitungan Pertama
        $datalatih_total = DB::table('diabetes')->get();
        $datalatih_total_positif = DB::table('diabetes')->where('Class', '1')->get();
        $datalatih_total_negatif = DB::table('diabetes')->where('Class', '0')->get();

        $jumlahdatatotal = count($datalatih_total);
        $jumlahdatapositif_total = count($datalatih_total_positif);
        $jumlahdatanegatif_total = count($datalatih_total_negatif);
        
        $Node1 = Diabetes::entropygain($fitur[$fitur1],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $Node2 = Diabetes::entropygain($fitur[$fitur2],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $Node3 = Diabetes::entropygain($fitur[$fitur3],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $array = [];
        $gain = [];
        array_push($array,$Node1);
        array_push($array,$Node2);
        array_push($array,$Node3);
        foreach ($array as $p) {
            array_push($gain,$p[2]);
        }
        $count=0;
        $gain_tertinggi = max($gain);
        foreach ($array as $p) {
            // DD($p[2],$gain_tertinggi);
            if ($p[2]!=$gain_tertinggi) {
                $count++;
                // DD($count);
            }else {
                break;
            }
        }
        
        
        #Perhitungan Kedua (Yes)
        $datalatih_total = DB::table('diabetes')->where($fitur[$fitur_use[$count]],'1')->get();
        $datalatih_total_positif = DB::table('diabetes')->where($fitur[$fitur_use[$count]],'1')->where('Class', '1')->get();
        $datalatih_total_negatif = DB::table('diabetes')->where($fitur[$fitur_use[$count]],'1')->where('Class', '0')->get();

        $jumlahdatatotal = count($datalatih_total);
        $jumlahdatapositif_total = count($datalatih_total_positif);
        $jumlahdatanegatif_total = count($datalatih_total_negatif);
        
        $entropy_total = -1*(($jumlahdatapositif_total/$jumlahdatatotal*(log($jumlahdatapositif_total/$jumlahdatatotal,2)))+($jumlahdatanegatif_total/$jumlahdatatotal*(log($jumlahdatanegatif_total/$jumlahdatatotal,2))));
        DD($entropy_total);

        $Node1 = Diabetes::entropygain($fitur[$fitur1],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $Node2 = Diabetes::entropygain($fitur[$fitur3],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $array = [];
        $gain = [];
        array_push($array,$Node1);
        array_push($array,$Node2);
        foreach ($array as $p) {
            array_push($gain,$p[2]);
        }
        $count=0;
        $gain_tertinggi = max($gain);
        foreach ($array as $p) {
            // DD($p[2],$gain_tertinggi);
            if ($p[2]!=$gain_tertinggi) {
                $count++;
                // DD($count);
            }else {
                break;
            }
        }
        DD($array);

        #Perhitungan Kedua (No)
        $datalatih_total = DB::table('diabetes')->where($fitur[$fitur_use[$count]],'0')->get();
        $datalatih_total_positif = DB::table('diabetes')->where($fitur[$fitur_use[$count]],'0')->where('Class', '1')->get();
        $datalatih_total_negatif = DB::table('diabetes')->where($fitur[$fitur_use[$count]],'0')->where('Class', '0')->get();

        $jumlahdatatotal = count($datalatih_total);
        $jumlahdatapositif_total = count($datalatih_total_positif);
        $jumlahdatanegatif_total = count($datalatih_total_negatif);
        
        

        $Node1 = Diabetes::entropygain($fitur[$fitur1],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $Node2 = Diabetes::entropygain($fitur[$fitur3],$jumlahdatatotal,$jumlahdatapositif_total,$jumlahdatanegatif_total);
        $array = [];
        $gain = [];
        array_push($array,$Node1);
        array_push($array,$Node2);
        foreach ($array as $p) {
            array_push($gain,$p[2]);
        }
        $count=0;
        $gain_tertinggi = max($gain);
        foreach ($array as $p) {
            // DD($p[2],$gain_tertinggi);
            if ($p[2]!=$gain_tertinggi) {
                $count++;
                // DD($count);
            }else {
                break;
            }
        }

        return redirect()->back();
    }  
    public function importdl(Request $request)
    {
        $dataset = DB::table('diabetes')->get();
        # validasi
		$request->validate([
			'data_latih' => 'required|mimes:csv,xls,xlsx'
		]);
		# menangkap file excel
		$file = $request->file('data_latih');
		# membuat nama file unik
		// $nama_file = rand().$file->getClientOriginalName();
 
		# upload ke folder file_siswa di dalam folder public
		// $file->move('datalatih',$nama_file);
 
		# import data ke database
        $array = Excel::toArray(new DatalatihImport, $file);
        foreach ($array[0] as $p) {
            DB::table('diabetes')->insert([
                'Age' => $p[0],
                'Gender' => $p[1],
                'Polyuria' => $p[2],
                'Polydipsia' => $p[3],
                'S_W_L' => $p[4],
                'Weakness' => $p[5],
                'Polyphagia' => $p[6],
                'Genital_thrus' => $p[7],
                'Visual_blur' => $p[8],
                'Itching' => $p[9],
                'Irritability' => $p[10],
                'Delayed_healing' => $p[11],
                'Partial_paresis' => $p[12],
                'Muscle_stiffness' => $p[13],
                'Alopecia' => $p[14],
                'Obesity' => $p[15],
                'Class' => $p[16],
                'created_at' => Carbon::now()
            ]);
        }
		// alihkan halaman kembali
		return redirect()->back();
    }    
}
