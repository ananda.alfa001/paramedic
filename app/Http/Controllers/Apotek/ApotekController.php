<?php

namespace App\Http\Controllers\Apotek;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApotekController extends Controller
{
    public function index()
    {
            $obat = DB::table('apotek')->get();
            return view('dashboard-apotek.apotek', compact('obat'));
    }
    public function gotokasir()
    {
        $obat = DB::table('apotek')->get();
        $list_obat = DB::table('invoice_list_obat')->get();
        return view('dashboard-apotek.pembelian ', compact('obat', 'list_obat'));
    }
    public function addkasir(Request $request, $id)
    {
        $obat = DB::table('apotek')->where('id', $id)->get();
        foreach ($obat as $p) {
            $stocknow = $p->Stok;
            $stockneed = $request->jumlah;
            $stockupdate = $stocknow - $stockneed;
            $transaksi = DB::table('invoice_list_obat')->insertGetId([
                'Nama_obat' => $p->Nama_obat,
                'Harga_Jual' => $p->Harga_Jual,
                'Stok_sebelumnya' => $stocknow,
                'Jumlah' => $request->jumlah,
                'created_at' => Carbon::now()
            ]);
            DB::table('apotek')->where('id', $id)->update([
                'Stok' => $stockupdate,
                'updated_at' => Carbon::now()
            ]);
        }
        return redirect('/dashboard-apotek/kasir');
    }
    public function deletekasir($id)
    {
        $obat = DB::table('invoice_list_obat')->where('id', $id)->get();
        foreach ($obat as $p) {
            DB::table('apotek')->where('Nama_obat', $p->Nama_obat)->update([
                'Stok' => $p->Stok_sebelumnya,
                'updated_at' => Carbon::now()
            ]);
        }
        DB::table('invoice_list_obat')->where('id', $id)->delete();
        return redirect('/dashboard-apotek/kasir');
    }
    public function sent_invoice(Request $request)
    {
        $list = DB::table('invoice_list_obat')->get();
        $invoice = DB::table('invoice_apotek')->max('id');
        $invoice++;
        $today = date("Ymd");
        $nextNoTransaksi = $today.sprintf('%04s', $invoice);
        $Invoice_Code = $nextNoTransaksi;
        // DD($Invoice_Code);
        DB::table('invoice_apotek')->insertGetId([
            'Kode_Invoice' => $Invoice_Code,
            'Total' => $request->total,
            'created_at' => Carbon::now()
        ]);
        foreach ($list as $p) {
            DB::table('invoice_obat')->insertGetId([
                'Kode_Invoice' => $Invoice_Code,
                'Nama_obat' => $p->Nama_obat,
                'Harga_Jual' => $p->Harga_Jual,
                'Jumlah' => $p->Jumlah,
                'created_at' => Carbon::now()
            ]);
        }
        
        DB::table('invoice_list_obat')->delete();
        return redirect('/dashboard-apotek/kasir');
    }
    public function gotoinvoice()
    {
        $obat = DB::table('invoice_apotek')->get();
        return view('dashboard-apotek.invoice_apotek ', compact('obat'));
    }
    public function stock()
    {
        $obat = DB::table('apotek')->get();
        return view('dashboard-apotek.stock', compact('obat'));
    }
    public function update_harga(Request $request, $id)
    {
        $obat = DB::table('apotek')->where('id', $id)->get();
        foreach ($obat as $p) {
            DB::table('kartu_stok')->insertGetId([
                'Golongan' => $p->Golongan,
                'Status' => "Update Harga",
                'Nama_obat' => $p->Nama_obat,
                'Harga_beli' => $p->Harga_beli,
                'Perubahan' => $request->input,
                'created_at' => Carbon::now()
            ]);
        }
        DB::table('apotek')->where('id', $id)
            ->update(
                [
                    'Harga_Jual' => $request->input
                ]
            );
        // DD($request->input);

        return redirect('/dashboard-apotek/lihatstok');
    }
    public function tambahstock(Request $request, $id)
    {
        $obat = DB::table('apotek')->where('id', $id)->get();
        $temp = 0;
        foreach ($obat as $p) {
            $temp = $p->Stok;
            DB::table('kartu_stok')->insertGetId([
                'Golongan' => $p->Golongan,
                'Status' => "Masuk",
                'Nama_obat' => $p->Nama_obat,
                'Harga_beli' => $p->Harga_beli,
                'Perubahan' => $request->input,
                'created_at' => Carbon::now()
            ]);
        }

        DB::table('apotek')->where('id', $id)
            ->update(
                [
                    'Stok' => $temp + $request->input,
                ]
            );
        return redirect('/dashboard-apotek/lihatstok');
    }
    public function hapus_obat($id)
    {
        $obat = DB::table('apotek')->where('id', $id)->get();
        foreach ($obat as $p) {
            DB::table('kartu_stok')->insertGetId([
                'Golongan' => $p->Golongan,
                'Status' => "Dihapus",
                'Nama_obat' => $p->Nama_obat,
                'Harga_beli' => $p->Harga_beli,
                'created_at' => Carbon::now()
            ]);
        }
        DB::table('apotek')->where('id', $id)
            ->delete();
        return redirect('/dashboard-apotek/lihatstok');
    }
    public function gotoadddrug()
    {
        return view('dashboard-apotek.tambahobat');
    }
    public function adddrug(Request $request)
    {
        $jumlah_box = $request->box;
        $jumlah_stripperbox = $request->strip;
        if ($jumlah_stripperbox == null) {
            $jumlah_stripperbox = 1;
        }
        $jumlah_bijiperstrip = $request->tablet;
        if ($jumlah_bijiperstrip == null) {
            $jumlah_bijiperstrip = 1;
        }
        $obat_bijian = $jumlah_bijiperstrip*$jumlah_stripperbox*$jumlah_box; 
        $obat = DB::table('apotek')->insertGetId([
            'Golongan' => $request->gol_obat,
            'Nama_obat' => $request->nama_obat,
            'Harga_beli' => $request->harga_beli,
            'Harga_Jual_Box' => $request->harga_jual_box,
            'Harga_Jual_Strip' => $request->harga_jual_strip,
            'Harga_Jual_Biji' => $request->harga_jual_biji,
            'satuan' => $request->sat_obat,
            'stripPerbox' => $jumlah_stripperbox,
            'bijiPerStrip' => $jumlah_bijiperstrip,
            'Stok' => $obat_bijian,
            'created_at' => Carbon::now()
        ]);
        DB::table('kartu_stok')->insertGetId([
            'Golongan' => $request->gol_obat,
            'Status' => "Obat Baru",
            'Nama_obat' => $request->nama_obat,
            'Harga_beli' => $request->harga_beli,
            'created_at' => Carbon::now()
        ]);
        return redirect('/dashboard-apotek');
    }
    public function bayar(Request $request, $id)
    {
        $invoice = DB::table('invoice_apotek')->where('id', $id)->get();
        $kembalian = 0;

        // DD($invoice);
        foreach ($invoice as $p) {
            $total = $p->Total;
            $temp = $request->bayar;
            $kembalian = $temp - $total;
        }
        DB::table('invoice_apotek')->where('id', $request->id)->update([
            'Bayar' => $request->bayar,
            'Kembalian' => $kembalian,
            'Status' => 'Lunas'
        ]);
        return redirect('/dashboard-apotek/invoice');
    }
    public function print($id)
    {
        $invoice = DB::table('invoice_apotek as b')->where('id', $id)->get();
        foreach ($invoice as $p) {
            $obat = DB::table('invoice_obat as b')->where('Kode_Invoice', $p->Kode_Invoice)->get();
        }
        // DD($obat);
        return view('dashboard-apotek.invoice_obat_print', compact('invoice', 'obat'));
    }
}
