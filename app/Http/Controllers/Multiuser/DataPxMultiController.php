<?php

namespace App\Http\Controllers\Multiuser;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Exports\RegisterKohortIbuExport;
use App\Exports\RekamMedisExport;
use App\Exports\StokApotekExport;
use App\Exports\KartuStokExport;
use App\Exports\InvoicePBMExport;
use App\Exports\InvoiceApotekExport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Alert;


class DataPxMultiController extends Controller
{

    public function Dashboard_Data(Request $request)
    {
        $pasien = DB::table('pasien')->get();
        if(Auth::user()->roles == "Admin"){
            return view('dashboard-admin.admin', compact('pasien'));
        }elseif(Auth::user()->roles == 'Pegawai'){
            return view('dashboard-pegawai.pegawai', compact('pasien'));
        }else{
            return redirect('/');
        }   
    }      

    public function daftar_berobat(Request $request)
    {
        if($request->id == null){
            $pasien = DB::table('pasien')->get();
            Alert::error('Daftar Gagal', 'Pilihan Tidak Terdaftar!');
            return view('dashboard-pegawai.pegawai', compact('pasien'));
        }else{
            $check = DB::table('pasien')->where('id', $request->id)->update([
                'Status_Px' => 1
            ]);
            if($check){
                DB::table('keluhan')->insert([
                    'id_px' => $request->id,
                    'keluhan' => $request->keluh,
                    'created_at' => Carbon::now()
                ]);
                Alert::success('Daftar Berhasil', 'Cek Antrian Pasien!');
                return redirect('/dashboard');
            }else{
                Alert::error('Daftar Gagal', 'Pilihan Terdapat Kesalahan!');
                return redirect('/dashboard');
            }
        }
    }
    
    public function tambah()
    {
        return view('dashboard-multiuser.patient_regist');
    }

    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'NIK' => 'required',
            'jenis_kelamin' => 'required',
            'nama_kk' => 'required',
            'alamat' => 'required',
            'nomor_telp' => 'required',
            'tgl_lhr' => 'required',
        ]);

        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $pasien = DB::table('pasien')->insertGetId([
                        'Nama_Px' => $request->nama,
                        'NIK' => $request->NIK,
                        'jenis_kelamin' => $request->jenis_kelamin,
                        'nama_KK' => $request->nama_kk,
                        'Alamat_Px' => $request->alamat,
                        'Nomor_Telepon' => $request->nomor_telp,
                        'Tanggal_Lahir' => $request->tgl_lhr,
                        'Status_Px' => 3,
                        'status_byr' => 1,
                        'created_at' => Carbon::now()
                    ]);
                    Alert::success('Tambah Pasien Berhasil', 'Cek Data Pasien!');
                    return redirect('/dashboard');
            }catch (\Exception $e) {
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return redirect()->back();
            }
        });
        return $post;
        }
    }

    public function daftar(){
        $pasien = DB::table('pasien as a')
                ->select('a.*', 'b.keluhan', 'b.status_keluhan')
                ->leftJoin('keluhan as b', 'a.id', 'b.id_px')
                ->get();
        return view('dashboard-multiuser.patient_sign', compact('pasien'));
    }

    public function queue(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'alamat' => 'required',
            'nomor_telp' => 'required',
            'tgl_lhr' => 'required',
            'status' => 'required',
            'keluh' => 'required'
        ]);
        
        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $pasien = DB::table('pasien')->insertGetId([
                        'Nama_Px' => $request->nama,
                        'Alamat_Px' => $request->alamat,
                        'Nomor_Telepon' => $request->nomor_telp,
                        'Tanggal_Lahir' => $request->tgl_lhr,
                        'Status_Px' => $request->status,
                        'created_at' => Carbon::now()
                    ]);
                    
                    DB::table('keluhan')->insert([
                        'id_px' => $pasien,
                        'keluhan' => $request->keluh,
                        'created_at' => Carbon::now()
                    ]);
                    Alert::success('Antrian Terdaftar', 'Cek Antrian Pasien!');
                    return redirect('/dashboard/patientsign');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }

    public function upstatuswait(Request $request)
    {
        $valid = Validator::make($request->all(), [
        ]);

        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    DB::table('pasien')->where('id', $request->id)->update([
                        'Status_Px' => 1
                    ]);
                    Alert::success('Update Status Berhasil', 'Cek Antrian Pasien!');
                    return redirect('/dashboard/patientsign');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }

    public function upstatusprogress(Request $request)
    {
        
        $valid = Validator::make($request->all(), [
        ]);

        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    DB::table('pasien')->where('id', $request->id)->update([
                        'Status_Px' => 2
                    ]);
                    Alert::success('Update Status Berhasil', 'Cek Antrian Pasien!');
                    return redirect('/dashboard/patientsign');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }

    public function upstatusdone(Request $request)
    {
        $valid = Validator::make($request->all(), [
        ]);

        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    DB::table('pasien')->where('id', $request->id)->update([
                        'Status_Px' => 3,
                        'status_byr' => 2
                    ]);
                    Alert::success('Update Status Berhasil', 'Cek Bagian Kasir!');
                    return redirect('/dashboard/patientsign');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }

    public function remed()
    {
        $pasien = DB::table('keluhan as a')
        ->select('b.*', 'a.keluhan', 'a.status_keluhan', 'a.tindakan', 'a.created_at')
        ->leftJoin('pasien as b', 'b.id', 'a.id_px')
        ->get();
        return view('dashboard-multiuser.rekam_medis', compact('pasien'));
    }

    public function kohort()
    {
        $pasien = DB::table('ibu_hamil as a')
        ->select('a.*', 'b.Nama_Px', 'b.NIK','b.nama_KK','b.Alamat_Px','b.Tanggal_Lahir')
        ->leftJoin('pasien as b', 'a.Id_Px', 'b.id')
        ->get();
        $tgl_lahir=null;
        foreach ($pasien as $p){
            $tgl_lahir = $p -> Tanggal_Lahir;
        }
        $now = Carbon::now();
        $b_day = Carbon::parse($tgl_lahir);
        $age = $b_day->diffInYears($now);
        // DD($pasien);
        return view('dashboard-multiuser.kohort', compact('pasien','age'));
    }

    public function kohortvalid($id)
    {
        $pasien = DB::table('ibu_hamil as a')
        ->select('a.*', 'b.Nama_Px', 'b.NIK','b.nama_KK','b.Alamat_Px','b.Tanggal_Lahir')
        ->leftJoin('pasien as b', 'a.Id_Px', 'b.id')
        ->where('a.id', $id)
        ->get();
        $tgl_lahir=null;
        foreach ($pasien as $p){
            $tgl_lahir = $p -> Tanggal_Lahir;
        }
        $now = Carbon::now();
        $b_day = Carbon::parse($tgl_lahir);
        $age = $b_day->diffInYears($now);  
        return view('dashboard-multiuser.ibu_hamil_validation', compact('pasien','age'));
    }
    
    public function gotoibuhamil($id)
    {
        $pasien = DB::table('pasien')->where('id', $id)->get();
        $tgl_lahir=null;
        foreach ($pasien as $p){
            $tgl_lahir = $p -> Tanggal_Lahir;
        }
        $now = Carbon::now(); 
        $b_day = Carbon::parse($tgl_lahir); 
        $age = $b_day->diffInYears($now); 
        return view('dashboard-multiuser.ibu_hamil',compact('pasien','age'));
    }

    public function setibuhamil(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'usia_hamil' => 'required',
            'sumber_biaya' => 'required',
            'hamil_ke' => 'required',
            'jarak_hamil' => 'required',
            'bb_tb' => 'required',
            'tekanan_darah' => 'required',
            'lila_imt' => 'required',
            'status_imunisasi' => 'required',
            'keterangan_imunisasi' => 'required',
        ]);

        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    DB::table('pasien')->where('id', $request->idpx)->update([
                        'Status_Px' => 1
                    ]);

                    DB::table('keluhan')->insert([
                        'id_px' => $request->idpx,
                        'keluhan' => $request->keluh,
                        'created_at' => Carbon::now()
                    ]);

                    DB::table('ibu_hamil')->insert([
                        'Id_Px' => $request->idpx,
                        'usia_kehamilan' => $request->usia_hamil,
                        'Sumber_Biaya' => $request->sumber_biaya,
                        'hamil_ke' => $request->hamil_ke,
                        'jarak_hamil' => $request->jarak_hamil,
                        'bb_tb' => $request->bb_tb,
                        'tekanan' => $request->tekanan_darah,
                        'lila_imt' => $request->lila_imt,
                        'status_imunisasi' => $request->status_imunisasi,
                        'keterangan_imunisasi' => $request->keterangan_imunisasi,
                        'Skrining_PE' => $request->Skrining_PE,
                        'Skrining_TB' => $request->Skrining_TB,
                        'Skrining_Jiwa' => $request->Skrining_Jiwa,
                        'Skrining_Gigi' => $request->Skrining_Gigi,
                        'Lab_Hb' => $request->Lab_Hb,
                        'Lab_Goldar' => $request->Lab_goldar,
                        'Lab_Protein' => $request->Lab_Protein,
                        'Lab_Glukosa' => $request->Lab_Glukosa,
                        'Lab_HIV' => $request->Lab_HIV,
                        'Lab_Sifilis' => $request->Lab_Sifilis,
                        'Lab_HBsAg' => $request->Lab_HBsAg,
                        'Lab_TBC' => $request->Lab_TBC,
                        'Lab_Malaria' => $request->Lab_malaria,
                        'Lab_Lain' => $request->Lab_Lain,
                        'Resiko_Nakes' => $request->resiko_nakes,
                        'Resiko_Masyarakat' => $request->resiko_masyarakat,
                        'tata_laksana_kasus' => $request->tlk,
                        'Buku_KIA' => $request->buku_kia,
                        'kunjungan_ibu' => $request->kunjungan_ibu,
                        'penolong_nakes' => $request->penolong_nakes,
                        'penolong_dukun' => $request->penolong_dukun,
                        'lahir_mati' => $request->lahir_mati,
                        'lahir_hidup' => $request->lahir_hidup,
                        'jadwal_nifas' => $request->jadwal_nifas,
                        'created_at' => Carbon::now()
                    ]);
                    Alert::success('Kohort Terdaftar', 'Cek Kohort Ibu Hamil!');
                    return redirect('/dashboard/patientsign');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }

    public function validateibuhamil(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'usia_hamil' => 'required',
            'sumber_biaya' => 'required',
            'hamil_ke' => 'required',
            'jarak_hamil' => 'required',
            'bb_tb' => 'required',
            'tekanan_darah' => 'required',
            'lila_imt' => 'required',
            'status_imunisasi' => 'required',
            'keterangan_imunisasi' => 'required',
        ]);

        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $pasien = DB::table('ibu_hamil')->where('id',$request->idkohort)->update([
                        'Id_Px' => $request->idpx,
                        'status_kohort' => 2,
                        'usia_kehamilan' => $request->usia_hamil,
                        'Sumber_Biaya' => $request->sumber_biaya,
                        'hamil_ke' => $request->hamil_ke,
                        'jarak_hamil' => $request->jarak_hamil,
                        'bb_tb' => $request->bb_tb,
                        'tekanan' => $request->tekanan_darah,
                        'lila_imt' => $request->lila_imt,
                        'status_imunisasi' => $request->status_imunisasi,
                        'keterangan_imunisasi' => $request->keterangan_imunisasi,
                        'Skrining_PE' => $request->Skrining_PE,
                        'Skrining_TB' => $request->Skrining_TB,
                        'Skrining_Jiwa' => $request->Skrining_Jiwa,
                        'Skrining_Gigi' => $request->Skrining_Gigi,
                        'Lab_Hb' => $request->Lab_Hb,
                        'Lab_Goldar' => $request->Lab_goldar,
                        'Lab_Protein' => $request->Lab_Protein,
                        'Lab_Glukosa' => $request->Lab_Glukosa,
                        'Lab_HIV' => $request->Lab_HIV,
                        'Lab_Sifilis' => $request->Lab_Sifilis,
                        'Lab_HBsAg' => $request->Lab_HBsAg,
                        'Lab_TBC' => $request->Lab_TBC,
                        'Lab_Malaria' => $request->Lab_malaria,
                        'Lab_Lain' => $request->Lab_Lain,
                        'Resiko_Nakes' => $request->resiko_nakes,
                        'Resiko_Masyarakat' => $request->resiko_masyarakat,
                        'tata_laksana_kasus' => $request->tlk,
                        'Buku_KIA' => $request->buku_kia,
                        'kunjungan_ibu' => $request->kunjungan_ibu,
                        'penolong_nakes' => $request->penolong_nakes,
                        'penolong_dukun' => $request->penolong_dukun,
                        'lahir_mati' => $request->lahir_mati,
                        'lahir_hidup' => $request->lahir_hidup,
                        'jadwal_nifas' => $request->jadwal_nifas,
                        'updated_at' => Carbon::now()
                    ]);
                    Alert::success('Kohort Tervalidasi', 'Cek Kohort Ibu Hamil!');
                    return redirect('/dashboard/kohort');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }

    public function export_RegisterKohortIbuExport($id) 
    {
        $time = Carbon::now();
        $year = Carbon::parse($time);
        $filename = $year->isoFormat('D-MMMM-Y'); 
        return Excel::download(new RegisterKohortIbuExport($id), 'Data-RegisterKohortIbu_'. $filename .'.xlsx');
    }

    public function export_RekamMedisExport() 
    {
        $time = Carbon::now();
        $year = Carbon::parse($time);
        $filename = $year->isoFormat('D-MMMM-Y'); 
        return Excel::download(new RekamMedisExport, 'Data-RekamMedis_' . $filename . '.xlsx');
    }
    
    public function export_invoicepbm() 
    {
        $time = Carbon::now();
        $year = Carbon::parse($time);
        $filename = $year->isoFormat('D-MMMM-Y'); 
        return Excel::download(new InvoicePBMExport, 'Invoice-PBM_' . $filename . '.xlsx');
    }

    public function export_invoiceapotek() 
    {
        $time = Carbon::now();
        $year = Carbon::parse($time);
        $filename = $year->isoFormat('D-MMMM-Y'); 
        return Excel::download(new InvoiceApotekExport, 'Invoice-Apotek_' . $filename . '.xlsx');
    }

    public function export_kartustok() 
    {
        $time = Carbon::now();
        $year = Carbon::parse($time);
        $filename = $year->isoFormat('D-MMMM-Y'); 
        return Excel::download(new KartuStokExport, 'Kartu-Stok-Apotek_' . $filename . '.xlsx');
    }

    public function export_stokapotek() 
    {
        $time = Carbon::now();
        $year = Carbon::parse($time);
        $filename = $year->isoFormat('D-MMMM-Y'); 
        return Excel::download(new StokApotekExport, 'Stok-Apotek_' . $filename . '.xlsx');
    }
}
