<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Alert;

class GuardController extends Controller
{
    public function AuthRoles(request $request)
    {   
        if(Auth::check()){
            if (Auth::user()->roles == 'Pegawai') {
                Alert::success('Login Success', 'Have a Nice Work Pegawai!');
                return redirect('dashboard-pegawai');
            }elseif(Auth::user()->roles == 'Apoteker'){
                Alert::success('Login Success', 'Have a Nice Work Apoteker!');
                return redirect('dashboard-apotek');
            }elseif(Auth::user()->roles == 'Admin'){
                Alert::success('Login Success', 'Have a Nice Work Admin!');
                return redirect('dashboard-admin');
            }else{
                Alert::error('Login Gagal', 'Coba Kembali!');
                return view('auth.login');
            }
        }else{
            Alert::error('Anda Belum Login', 'Silahkan Login Dahulu!');
            return view('auth.login');
        }
    }
    public function AuthGuard(request $request)
    {   
        if(Auth::check()){
            if (Auth::user()->roles == 'Pegawai') {
                Alert::success('Login Success', 'Have a Nice Work Pegawai!');
                return redirect('dashboard-pegawai');
            }elseif(Auth::user()->roles == 'Apoteker'){
                Alert::success('Login Success', 'Have a Nice Work Apoteker!');
                return redirect('dashboard-apotek');
            }elseif(Auth::user()->roles == 'Admin'){
                Alert::success('Login Success', 'Have a Nice Work Admin!');
                return redirect('dashboard-admin');
            }else{
                Alert::error('Login Gagal', 'Coba Kembali!');
                return view('auth.login');
            }
        }else{
            Alert::error('Anda Belum Login', 'Silahkan Login Dahulu!');
            return view('auth.login');
        }
    }
}
