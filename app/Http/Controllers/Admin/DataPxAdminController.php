<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DataPxAdminController extends Controller
{
    public function index()
    {
        // mengambil data dari table pegawai
        $pasien = DB::table('pasien')->get();

        // mengirim data pegawai ke view index
        return view('dashboard-multiuser.patient_table', compact('pasien'));

        // return $nama;
    }
    public function gotokartustok()
    {
        $obat = DB::table('kartu_stok')->get();
        $stok = DB::table('apotek')->get();
        // DD($pasien);
        return view('dashboard-apotek.kartu_stock', compact('obat','stok'));
    }
    public function edit($id)
    {
        // mengambil data pegawai berdasarkan id yang dipilih
        $pasien = DB::table('pasien as a')
        ->where('a.id', $id)
        ->get();
        
        // passing data pegawai yang didapat ke view edit.blade.php
        return view('dashboard-multiuser.patient_update', compact('pasien'));
    }
    public function hapus($id)
    {
        // mengambil data pegawai berdasarkan id yang dipilih
        $pasien = DB::table('pasien')->where('id', $id)->delete();
        //DD($pasien);
        //DB::table('pasien')->delete('id', $id);
        
        // passing data pegawai yang didapat ke view edit.blade.php
        return redirect('/dashboard-admin/patienttable');
    }
    
    public function update_profile(Request $request, $id)
    {
        // update data pegawai
        $pasien = DB::table('pasien')->where('id', $request->id)->update([
            'Nama_Px' => $request->nama,
            'nama_KK' => $request->kk,
            'jenis_kelamin' => $request->jenkel,
            'Alamat_Px' => $request->alamat,
            'Nomor_Telepon' => $request->nomor_telp,
            'Tanggal_Lahir' => $request->tgl_lhr,
            'updated_at' => Carbon::now()
        ]);
        
        
        
        // alihkan halaman ke halaman dashboard
        return redirect('/dashboard-admin');
    }
}
