<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function Dashboard_Data_Pasien()
    {
        $pasien = DB::table('pasien')->paginate(10);
        return view('dashboard-admmin.admin', compact('pasien'));
    }
    public function index()
    {
        if(Auth::user()->roles == "Admin"){
            $pasien = DB::table('pasien')->paginate(10);
            return view('dashboard-admin.admin', compact('pasien'));
        }else{
            return redirect('/');
        }   
    }
}