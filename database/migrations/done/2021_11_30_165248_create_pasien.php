<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien', function (Blueprint $table) {
            $table->id();
            $table->string('NIK');
            $table->string('Nama_Px');
            $table->string('jenis_kelamin');
            $table->string('nama_KK');
            $table->string('Alamat_Px');
            $table->string('Nomor_Telepon');
            $table->date('Tanggal_Lahir');
            $table->integer('Status_Px');
            $table->integer('status_byr');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
