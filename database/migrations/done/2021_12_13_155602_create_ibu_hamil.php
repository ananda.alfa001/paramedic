<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIbuHamil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ibu_hamil', function (Blueprint $table) {
            $table->id();
            $table->integer('Id_Px');
            $table->integer('status_kohort')->default("1");
            $table->string('Sumber_Biaya');
            $table->integer('usia_kehamilan');
            $table->integer('hamil_ke');
            $table->integer('jarak_hamil');
            $table->string('bb_tb');
            $table->string('tekanan'); 
            $table->string('lila_imt');
            $table->string('status_imunisasi');
            $table->string('keterangan_imunisasi');
            $table->string('Skrining_PE')->nullable();
            $table->string('Skrining_TB')->nullable();
            $table->string('Skrining_Jiwa')->nullable();
            $table->string('Skrining_Gigi')->nullable();
            $table->string('Lab_Hb')->nullable();
            $table->string('Lab_Goldar')->nullable();
            $table->string('Lab_Protein')->nullable();
            $table->string('Lab_Glukosa')->nullable();
            $table->string('Lab_HIV')->nullable();
            $table->string('Lab_Sifilis')->nullable();
            $table->string('Lab_HBsAg')->nullable();
            $table->string('Lab_TBC')->nullable();
            $table->string('Lab_Malaria')->nullable();
            $table->string('Lab_Lain')->nullable();
            $table->string('Resiko_Nakes')->nullable();
            $table->string('Resiko_Masyarakat')->nullable();
            $table->string('tata_laksana_kasus')->nullable();
            $table->string('Buku_KIA')->nullable();
            $table->string('kunjungan_ibu')->nullable();
            $table->string('penolong_nakes')->nullable();
            $table->string('penolong_dukun')->nullable();
            $table->string('lahir_mati')->nullable();
            $table->string('lahir_hidup')->nullable();
            $table->string('jadwal_nifas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ibu_hamil');
    }
}
