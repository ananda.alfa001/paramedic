<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Diabetes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diabetes', function (Blueprint $table) {
            $table->id();
            $table->string('Age');
            $table->string('Gender');
            $table->integer('Polyuria');
            $table->integer('Polydipsia');
            $table->integer('S_W_L');
            $table->integer('Weakness');
            $table->integer('Polyphagia');
            $table->integer('Genital_thrus');
            $table->integer('Visual_blur');
            $table->integer('Itching');
            $table->integer('Irritability');
            $table->integer('Delayed_healing');
            $table->integer('Partial_paresis');
            $table->integer('Muscle_Stiffness');
            $table->integer('Alopecia');
            $table->integer('Obesity');
            $table->integer('Class');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
