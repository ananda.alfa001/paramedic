<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceApotek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_apotek', function (Blueprint $table) {
            $table->id();
            $table->string('Kode_Invoice');
            $table->string('Status')->default("Belum Lunas");
            $table->integer('Total');
            $table->integer('Bayar')->default(0);
            $table->integer('Kembalian')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_apotek');
    }
}
