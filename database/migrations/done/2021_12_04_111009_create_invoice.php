<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->id();
            $table->string('Kode_Invoice');
            $table->string('id_Px');
            $table->string('pengobatan');
            $table->string('inap');
            $table->string('ops_invoice');
            $table->integer('infus');
            $table->integer('spuit');
            $table->integer('incisi');
            $table->integer('rawat_luka');
            $table->integer('jahit_luka');
            $table->integer('angkat_jahit');
            $table->integer('kuku');
            $table->integer('cateter');
            $table->integer('khitan');
            $table->integer('persalinan');
            $table->integer('cairan');
            $table->integer('surat_istirahat');
            $table->integer('obat_tablet');
            $table->integer('injeksi');
            $table->integer('suposutoria');
            $table->integer('konsultasi');
            $table->integer('lab');
            $table->integer('lain');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_invoice');
    }
}
